let hash = ref (None : string option)
let sign = ref 1L

let arg_list = [
  "--hash", Arg.String (fun s -> hash := Some s), "Block hash";
  "--sign", Arg.Int (fun i -> sign := Int64.of_int i), "Sign of update" ]

let arg_usage = "./update-counts [ARGUMENTS] update counts for a block"

let () =
  Arg.parse arg_list (fun _ -> ()) arg_usage;
  match !hash with
  | Some hash -> Writer.update_counts hash !sign
  | _ ->
    Printf.eprintf "No block hash given.\n%!";
    exit 2
