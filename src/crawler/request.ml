(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types
open EzAPI.TYPES

exception NoCurlResult of string

let spf = Printf.sprintf

let chain_id = "main"
let root = spf "/chains/%s" chain_id
let head = "head"
let genesis = "genesis"

let debug fmt = Utils.debug !Debug_constants.debug_request fmt

let request ~post ?(data="{}") url path =
  let url = Printf.sprintf "%s:%d%s" url.address url.port path in
  (* a hack because EzCurl to keep the requests synchronous to avoid
     changing too much code.*)
  let result = ref None in
  let on_success r = result := Some r in
  try
    let url_t = URL url in
    if post then EzCurl.post ~content:data "" url_t on_success
    else EzCurl.get "" url_t on_success;
    match !result with
    | Some json -> json
    | None -> raise (NoCurlResult url)
  with exn -> raise exn

let cachable = true

let request ?(post=false) ~cachable url path =
  Cache.with_cache ~debug:"crawler" ~cachable ~key:path ~request:(fun () ->
      request ~post ~data:"{}" url path)

(* We cache baking_rights and other ones even if block=head, because
   they are not supposed to change between two different heads. *)

let level url block =
  let path = spf "%s/blocks/%s/helpers/current_level" root block in
  debug "[Request] level %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.Level.encoding @@
  request ~cachable:(block <> head) url path


let operations url block =
  let path = spf "%s/blocks/%s/operations" root block in
  debug"[Request] operations %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.Operation.encoding @@
  request ~cachable:(block <> head) url path

let pending_operations url =
  let path = spf "%s/mempool/pending_operations" root in
  debug "[Request] pending_operations %s\n%!" path;
  EzEncoding.destruct Dune_encoding.Mempool.encoding @@
  request ~cachable:false url path

let predecessor url block =
  (* Should be this RPC, it is not registered properly in the node *)
  (* let path = spf "%s/blocks/%s/header/shell/predecessor" root hash in *)
  let path = spf "%s/blocks/%s/header/shell" root block in
  debug "[Request] predecessor %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.Predecessor.encoding @@
  request ~cachable:(block <> head) url path

let block url hash =
  let path = spf "%s/blocks/%s" root hash in
  debug "[Request] block %s\n%!" path ;
  let buf = request ~cachable:(hash <> head) url path in
  EzEncoding.destruct Dune_encoding.Block.encoding buf

let genesis url =
  let path = spf "%s/blocks/%s" root genesis in
  debug "[Request] genesis %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.Block.genesis_encoding @@
  request ~cachable url path

let block_first url =
  let path = spf "%s/blocks/1" root in
  debug "[Request] first block %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.Block.genesis_encoding @@
  request ~cachable url path

let get_head_hash ?(block=head) url =
  let path = spf "%s/blocks/%s/hash" root block in
  (* HACK !!!!  *)
  let block_hash = request ~cachable:(block <> head) url path in
  String.sub block_hash 1 (String.length block_hash - 3)

let get_alternative_heads_hashes url =
  let path = spf "%s/blocks" root in
  EzEncoding.destruct
    Dune_encoding.Alternative_blocks.encoding @@
  request ~cachable:false url path

(* Shortcuts on [head] *)
let current_level ?(block=head) url = level url block
let head_block ?(block_hash=head) url = block url block_hash

let node_contract url block hash =
  let path =
    spf
      "%s/blocks/%s/context/contracts/%s" root block hash in
  (* debug "[crawler] node_balance %s\n%!" path ; *)
  EzEncoding.destruct
    Dune_encoding.Account.encoding @@
  request ~cachable:(block <> head) url path

let node_contracts url bhash =
  let path = spf "%s/blocks/%s/context/contracts/" root bhash in
  let contracts =
    EzEncoding.destruct Dune_encoding.Contracts.encoding @@
    request ~cachable:(bhash<>head) url path in
  List.map (fun contract ->
      let {Dune_types.node_acc_balance; node_acc_spendable; node_acc_dlgt; _} =
        node_contract url bhash contract in
      let (delegatable, delegate) = node_acc_dlgt in
      contract, node_acc_balance, node_acc_spendable, delegatable, delegate)
    contracts

let coingecko_tickers () =
  let url = {address = "https://api.coingecko.com"; port = 443} in
  let path = "/api/v3/coins/dune/tickers" in
  debug "[Request] coingecko info %s\n%!" path ;
  EzEncoding.destruct Api_encoding.Coingecko.encoding @@
  request ~cachable:false url path

let network_stats url =
  let path = "/network/peers" in
  debug "[Request] network_stats %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.Network.encoding @@
  request ~cachable:false url path

let marketcap url =
  let path = "/v1/ticker/dune/" in
  EzEncoding.destruct Api_encoding.V1.MarketCap.encoding @@
  request ~cachable:false url path

let constants url block =
  let path = spf "%s/blocks/%s/context/constants" root block in
  debug "[Request] constants %s\n%!" path ;
  EzEncoding.destruct Dune_encoding.constants @@ request ~cachable:false url path
