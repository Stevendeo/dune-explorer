open Data_types
open Dune_types_min

let start = ref 0
let last = ref (-1)
let addr = ref { address = "http://localhost"; port = 8732 }
let level_file : string option ref = ref None
let levels : int list ref = ref []

let speclist = [
  "--addr", Arg.String (fun s -> addr := {!addr with address = s}), "node address";
  "--port", Arg.Int (fun i -> addr := {!addr with port = i}), "node port";
  "--start", Arg.Set_int start, "starting level";
  "--end", Arg.Set_int last, "ending level";
  "--levels", Arg.String (fun s -> level_file := Some s), "list of levels"
]

let dbh = PGOCaml.connect ~database:DunscanConfig.database ()

let crawl init loop finish =
  let rec iter state = function
    | i when i > !last || i < !start -> state
    | i -> iter (loop state i) (i+1) in
  finish (iter (init ()) !start)

let init state = state

let loop _state i =
  let block = Request.block !addr (string_of_int @@ List.nth !levels i) in
  let block_hash = block.node_hash in
  let operations = List.nth block.node_operations 3 in
  let get_man s acc = function
    | None -> acc
    | Some m -> match m.manager_meta_operation_result with
      | None -> acc
      | Some m -> (s, m) :: acc in
  let _get_anon acc = function None -> acc | Some m -> m :: acc in
  let get_meta acc = function
    | NTransaction tr -> get_man "tr" acc tr.node_tr_metadata
    | NOrigination ori -> get_man "or" acc ori.node_or_metadata
    | NReveal rvl -> get_man "rv" acc rvl.node_rvl_metadata
    | NDelegation del -> get_man "dl" acc del.node_del_metadata
    (* | NActivation act -> get_anon acc act.node_act_metadata
     * | NEndorsement endo -> get_anon acc endo.node_endorse_metadata
     * | NProposals pr -> get_anon acc pr.node_prop_metadata
     * | NBallot ba -> get_anon acc ba.node_ballot_metadata
     * | NSeed_nonce_revelation sn -> get_anon acc sn.node_seed_metadata *)
    | _ -> acc in
  let f = List.fold_left get_meta [] in
  let g op_hash l = List.iter (fun (s, meta) ->
      List.iter (fun {node_err_kind; node_err_id; node_err_info} ->
          Printf.printf "update operation %s of block %s\n%!" op_hash block_hash;
          let arr = [ Some node_err_id; Some node_err_kind; Some node_err_info ] in
          if s = "tr" then
            ignore(PGSQL(dbh) "update transaction_all \
                               set errors = array_cat(coalesce(errors, '{}'), $arr) \
                               where hash = $op_hash and op_block_hash = $block_hash")
          else if s = "or" then
            ignore(PGSQL(dbh) "update origination_all \
                               set errors = array_cat(coalesce(errors, '{}'), $arr) \
                               where hash = $op_hash and op_block_hash = $block_hash")
          else if s = "dl" then
            ignore(PGSQL(dbh) "update delegation_all \
                               set errors = array_cat(coalesce(errors, '{}'), $arr) \
                               where hash = $op_hash and op_block_hash = $block_hash")
          else if s = "rv" then
            ignore(PGSQL(dbh) "update reveal_all \
                               set errors = array_cat(coalesce(errors, '{}'), $arr) \
                               where hash = $op_hash and op_block_hash = $block_hash")
        ) meta.meta_op_errors) l in
  List.iter (fun op -> g op.node_op_hash (f op.node_op_contents)) operations

let finish _state = ()

let () =
  Arg.parse speclist (fun _ -> ()) "Fast crawler";
  crawl init loop finish
