(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open StringCompat
open Data_types
open Dune_types
open Options

let count_exchange = ref 9
let extras_sleep = 15
let marketcap_url =
  { address = "https://api.coinmarketcap.com" ; port = 443 }

let () =
  Options.speclist :=
    [
      "--extra-data",
      Arg.Unit (fun () -> crawler := "extra-data"),
      " Run a crawler for extras information \
       only (marketcap, network stats, votes/ballot)"  ] @
    ! Options.speclist

let analyze_exn name = function
  (* Try next time, maybe the node is broken, or we need to edit
     Data_encoding with the broken json *)
  | EzEncoding.DestructError ->
    debug "[Crawler][%s] Encoding error, need to update encodings\n%!" name
  (* Try next time with another node (maybe this one timeout, or is
     just unavailable. *)
  | Curl.CurlException (_code, i , s) ->
    debug "[Crawler][%s] Curl Error %d %s, probably neet to wait a bit longer or \
           try with another node\n%!" name i s
  (* Something goes wrong with the current node, we should definitely
     do something, but for now try with another node. *)
  | Request.NoCurlResult path ->
    debug "[Crawler][%s] No Curl Result for %S\n%!" name path
  | Failure msg ->
    debug "[Crawler][%s] Catch failure %S\n%!" name msg
  | Assert_failure (s, l, c) ->
    debug "[Crawler][%s] Assert failure: %s at line %d, column %d\n%!" name s l c
  | exn ->
    debug "[Crawler][%s] Unexpected failure %S\n%!" name (Printexc.to_string exn);
    assert false

let filter_config config url =
  let nodes =
    if List.length config.nodes > 1 then
      List.filter (fun { address ; port = _ } -> address <> url.address) config.nodes
    else config.nodes in
  { config with nodes }

let analyze_exn_switch name f config url = function
  (* Try next time, maybe the node is broken, or we need to edit
     Data_encoding with the broken json *)
  | EzEncoding.DestructError ->
    debug "[Crawler][%s] Encoding error, need to update encodings\n%!" name;
    f config
  (* Try with another node, this one not responding *)
  | Curl.CurlException (_code, i, s) ->
    debug "[Crawler][%s] Curl Error %i %s, probably neet to wait a bit longer or \
           try with another node\n%!" name i s;
    (* Try to remove locally this node, maybe it will respond later *)
    f (filter_config config url)
  | Request.NoCurlResult path ->
    debug "[Crawler][%s] No Curl Result for %S\n%!" name path;
    f (filter_config config url)
  | Failure msg ->
    debug "[Crawler][%s] Catch failure %S\n%!" name msg;
    f (filter_config config url)
  | exn ->
    debug "[Crawler][%s] Unexpected failure %S\n%!" name (Printexc.to_string exn);
    assert false

let get_marketcap () =
  try
    let marketcaps = Request.marketcap marketcap_url in
    Writer.register_marketcap marketcaps
  with exn -> analyze_exn "marketcap" exn

let update_exchanges () =
  if !count_exchange >= 10 then (
    count_exchange := 0;
    try
      let gecko_tickers = Request.coingecko_tickers () in
      List.iter Writer.register_coingecko_exchange gecko_tickers
    with exn -> analyze_exn "coingecko" exn
  )
  else
    incr count_exchange

let main_extras config =
  let url = choose_node config in
  try
    (* Registering market cap information on coinmarketcap *)
    (* debug "[Crawler] Gathering coinmarketcap information\n%!" ;
     * get_marketcap () ; *)

    (* Registering network stats information about peers *)
    debug "[Crawler] Gathering network stats information\n%!" ;
    let network_stats = Request.network_stats url in
    Dbw.register_network_stats network_stats ;

    (* let level = Request.current_level ?block:(!new_head) url in *)
    (* voting rolls *)
    (* debug "[Crawler] Gathering voting/amendment data\n%!" ;
     * get_voting_rolls url level; *)

    (* update list of exchanges *)
    (* update_exchanges () *)
  with exn -> analyze_exn "main-extra-public" exn

let filter_config url config =
  let nodes =
    List.filter (fun { address ; port = _ } -> address <> url.address)
      config.nodes in
  {config with nodes}

let rec register_genesis config =
  let url = choose_node config in
  try
    let genesis = Request.genesis url in
    if not @@ Dbw.is_block_registered genesis.node_hash then begin
      Dbw.register_genesis genesis
    end
  with exn -> analyze_exn_switch "genesis" register_genesis config url exn

let get_constants url level f = f @@ Request.constants url (string_of_int level)

let rec register_first config =
  let url = choose_node config in
  try
    let block = Request.block_first url in
    if (not @@ Dbw.is_block_registered block.node_hash) &&
       Dbw.is_block_registered block.node_header.header_shell.shell_predecessor
    then begin
      let level = Request.level url block.node_hash in
      Dbw.register_block ~constants:(get_constants url) block level 0L 0L 0L 0L;
      let date = block.node_header.header_shell.shell_timestamp in
      let first_contracts = Request.node_contracts url block.node_hash in
      List.iter (fun (hash, balance, spendable, delegatable, delegate) ->
          Dbw.register_init_balance hash balance (Utils.dune_to_data_date date)
            block.node_header.header_shell.shell_level;
          Dbw.register_dune_user hash;
          begin match delegate with
            | None -> ()
            | Some delegate -> Dbw.register_dune_user delegate end;
          Dbw.update_originated_contract hash delegate hash spendable delegatable
        ) first_contracts
    end
  with exn -> analyze_exn_switch "register first" register_first config url exn

let rec register main config hash =
  let predecessor_hash = Request.predecessor config hash in
  let level = Request.level config hash in
  if !quit then exit 1 ;
  if (not !Options.wait_rights || Writer.has_level_rights level.node_lvl_level) then (
    begin
      if not @@ Dbw.is_block_registered predecessor_hash then begin
        debug "[Crawler] Found block %s\n%!" predecessor_hash ;
        register main config predecessor_hash
      end
    end ;
    if !quit then exit 1 ;
    if not @@ Dbw.is_block_registered hash then begin
      (* Register head
         - register protocol
         - register test protocol
         - register block
         - register detailed operations (endorsement, transaction, etc) *)

      debug "[Crawler] Found level %d\n%!" level.node_lvl_level ;
      let block = Request.block config hash in
      let operations = List.flatten @@ block.node_operations in
      debug "[Crawler] Found operations %d\n%!" @@ List.length operations ;
      let t1 = Unix.gettimeofday () in
      debug "[Crawler] [%d] Registering block %s\n%!" block.node_header.header_shell.shell_level hash ;
      Dbw.register_all ~constants:(get_constants config) block level operations;
      if main then
        Dbw.register_main_chain !count block;
      let t2 = Unix.gettimeofday () in
      debug "[Crawler] [%d] Ok in %fs\n%!" block.node_header.header_shell.shell_level (t2 -. t1)
    end)
  else (
    debug "[Crawler] wait rights for level %d\n%!" level.node_lvl_level_position;
    Unix.sleep extras_sleep)

let make_catchup_levels url hash level =
  let result = ref [] in
  let nb_steps = level / !catchup_step in
  (* Computing max_steps by looking at the last registered block in the db *)
  let max_steps =
    match Dbw.head () with
    | None -> nb_steps
    | Some block -> (level - block.level) / !catchup_step in
  for i = 0 to max_steps do
    let i = i * !catchup_step in
    let block = Request.block url (Printf.sprintf "%s~%d" hash i) in
    result := (block.node_hash, block.node_header.header_shell.shell_level) :: !result
  done ;
  !result

let main_chain config =
  let url = choose_node config in
  try
    let register_or_not ?(main = false) url hash =
      if not @@ Dbw.is_block_registered hash then begin
        (* New block found ! *)
        let node_level = Request.level url hash in
        let current_level = node_level.node_lvl_level in
        let catchup_levels = make_catchup_levels in
        List.iter (fun (hash, level) ->
            debug "[Crawler] ========= \
                   Catching block %s at level %d / %d \
                   =========\n%!" hash level current_level;
            register main url hash)
          (catchup_levels url hash current_level)
      end
      (* Now we are up to date, we can register alt heads *)
      else
        alternative_heads_flag := true in
    (* Try to register the current head (if predecessor doesn't exist,
       try to get it first recursively). If until_mode is enabled, use
       the hash given in CLI as new head. *)
    let head_hash =
      match !new_head with
      | None ->
        Request.get_head_hash ?block:(!new_head) url
      | Some hash -> hash in
    (* Wait to be up to date before registering alt head (Cf. issue
       when catching new chain from scratch) *)
    alternative_heads_flag := false ;
    register_or_not ~main:true url head_hash ;
    debug "[Crawler] Registered block %s\n%!" head_hash ;
    if !until_mode then exit 1 ;

    if !alternative_heads_flag then begin
      (* Registering alternative heads and operations in their branches *)
      match Request.get_alternative_heads_hashes url with
      | [] -> ()
      | _ :: heads -> (* Ignore the first block *)
        debug "[Crawler] Found heads %d\n%!" @@ List.length heads ;
        List.iter (register_or_not url) @@ List.flatten heads ;
        debug "[Crawler] Registered heads\n%!" ;
    end;

    if !pending_operations_flag then begin
      (* Registering pending operations *)
      try
        let pending_operations = Request.pending_operations url in
        debug "[Crawler] Found pending_operations %d\n%!" @@
        List.length pending_operations.applied ;
        Writer.register_pending (CalendarLib.Calendar.now ()) pending_operations
      with exn -> analyze_exn "pending-operations" exn
    end;
    Dbw.register_crawler_activity "chain" sleep

  with exn -> analyze_exn "main" exn

let main_msg = "main operations"

let main_init () =
  if !count && (!start_level_count <> -1) then
    Dbw.counts_downup !start_level_count !end_level_count;
  let config = get_crawler () in
  register_genesis config;
  register_first config;
  Infos.init_constants @@ Writer.constants ()

let extras_msg = "extra informations"

let init_extras () =
  Config.load_config_api ~crawler:"extra-data" (Some !Options.config_file)

let init () =
  crawlers := StringMap.add
      "main" (main_init, main_chain, sleep, main_msg) !crawlers ;
  crawlers := StringMap.add
      "extra-data" (init_extras, main_extras, extras_sleep, extras_msg ) !crawlers
