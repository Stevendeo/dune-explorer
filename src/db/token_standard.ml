open Data_types_min

let parse_micheline_encoding caller e =
  let dummy_fees = 0L in
  let ts_caller = Alias.to_name caller in
  match e with
  | Prim ("Right", [Prim ("Left", [String addr], _)], _) -> TS_Kyc addr
  | Prim ("Left", [Prim ("Pair", [String dst; Int ts_amount], _)], _) ->
    TS_Transfer
      { ts_src = ts_caller; ts_dst = Alias.to_name dst; ts_amount;
        ts_fee = dummy_fees; ts_flag = "ds20-1"}
  | Prim ("Right", [Prim ("Right", [Prim ("Left", [Prim ("Pair", [
      String src; Prim ("Pair", [String dst; Int ts_amount], _)], _)], _)], _)], _) ->
    TS_TransferFrom
      { ts_src = Alias.to_name src; ts_dst = Alias.to_name dst; ts_amount;
        ts_fee = dummy_fees; ts_flag = "ds20-1" }
  | Prim ("Right", [Prim ("Left", [Prim ("Pair", [String dst; Int ts_amount], _)], _)], _) ->
    TS_Approve
        { ts_src = ts_caller ; ts_dst = Alias.to_name dst; ts_amount;
          ts_fee = dummy_fees; ts_flag = "ds20-1"}
    | _ -> TS_Unknown

let decode caller s =
  parse_micheline_encoding caller
    (Api_encoding.Micheline.decode s)

let token_op_to_str = function
  | TS_Unknown -> "unknown"
  | TS_Kyc _ -> "kyc"
  | TS_Transfer _ -> "transfer"
  | TS_TransferFrom _ -> "transferFrom"
  | TS_Approve _ -> "approve"
