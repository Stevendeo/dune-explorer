(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

let downgrade_1_to_0 = []

module Constants = struct
  let last_endorsements = 100L
  let last_transactions = 1000L
  let last_originations = 1000L
  let last_delegations = 1000L
  let last_activations = 1000L
  let last_reveals = 1000L
end

let update_0_to_1 dbh version =
  EzPG.upgrade ~verbose:true ~dbh ~version ~downgrade:downgrade_1_to_0 [
    {|ALTER ROLE SESSION_USER SET search_path TO db, public|};

    {|CREATE DOMAIN hash varchar|};
    {|CREATE DOMAIN protocol_hash hash|};
    {|CREATE DOMAIN user_hash hash|};
    {|CREATE DOMAIN block_hash hash|};
    {|CREATE DOMAIN operation_hash hash|};
    {|CREATE DOMAIN user_hashes varchar[]|};

    {|CREATE TABLE protocol(
      hash protocol_hash PRIMARY KEY,
      name VARCHAR NOT NULL)|};

    {|CREATE TABLE tezos_user (
      id bigserial,
      hash user_hash PRIMARY KEY,
      contract boolean NOT NULL,
      edpk user_hash,
      alias varchar)|};

    {|CREATE INDEX tezos_user_index_hash ON tezos_user(hash)|};
    {|CREATE INDEX tezos_index_contract ON tezos_user (contract)|};

    {|CREATE TABLE block (
      hash block_hash PRIMARY KEY,
      predecessor block_hash NOT NULL REFERENCES block (hash) DEFERRABLE INITIALLY DEFERRED,
      fitness hash NOT NULL,
      baker user_hash NOT NULL REFERENCES tezos_user(hash),
      timestamp timestamp NOT NULL,
      protocol protocol_hash REFERENCES protocol (hash) NOT NULL,
      test_protocol protocol_hash REFERENCES protocol (hash) NOT NULL,
      network hash NOT NULL,
      test_network hash NOT NULL,
      test_network_expiration VARCHAR NOT NULL,
      level bigint NOT NULL,
      level_position bigint NOT NULL,
      priority integer NOT NULL DEFAULT 0,
      cycle bigint NOT NULL,
      cycle_position bigint NOT NULL,
      voting_period bigint NOT NULL,
      voting_period_position bigint NOT NULL,
      commited_nonce_hash hash NOT NULL,
      pow_nonce hash NOT NULL,
      distance_level bigint NOT NULL,
      operation_count bigint NOT NULL,
      validation_pass bigint NOT NULL,
      proto bigint NOT NULL,
      data varchar NOT NULL,
      signature varchar NOT NULL,
      volume bigint NOT NULL DEFAULT 0,
      fees bigint NOT NULL DEFAULT 0,
      voting_period_kind varchar NOT NULL)|};

    {|CREATE INDEX block_time ON block (timestamp)|};
    {|CREATE INDEX block_predecessor ON block (predecessor)|};
    {|CREATE UNIQUE INDEX block_main_level ON block (level) WHERE distance_level = 0|};
    {|CREATE INDEX block_main_baker ON block (baker) WHERE distance_level = 0|};
    {|CREATE INDEX block_index_distance_level ON block (distance_level)|};
    {|CREATE INDEX block_index_level ON block (level)|};
    {|CREATE INDEX block_index_baker ON block (baker)|};
    {|CREATE INDEX block_timestamp_index ON block (timestamp)|} ;

    {|CREATE TYPE operation_type AS ENUM (
      'seed_nonce_revelation',
      'faucet',
      'transaction',
      'origination',
      'delegation',
      'endorsement',
      'proposal',
      'ballot',
      'activate',
      'activate_testnet')|};

    {|CREATE TABLE operation (
      hash operation_hash PRIMARY KEY,
      op_type VARCHAR NOT NULL,
      timestamp timestamp NOT NULL,
      op_anon_type text[],
      op_manager_type text[])|};

    {|CREATE INDEX operation_timestamp_index ON operation (timestamp)|} ;

    {|CREATE TABLE block_operation (
      block_hash block_hash NOT NULL REFERENCES block (hash),
      operation_hash operation_hash NOT NULL REFERENCES operation(hash))|};

    {|CREATE INDEX block_operation_operation ON block_operation (operation_hash)|};
    {|CREATE INDEX block_operation_block ON block_operation (block_hash)|};


    {|CREATE TABLE seed_nonce_revelation (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      level bigint NOT NULL,
      nonce VARCHAR NOT NULL,
      PRIMARY KEY (hash, level))|};

    {|CREATE INDEX seed_nonce_revelation_hash ON seed_nonce_revelation (hash)|};

    {|CREATE TABLE faucet (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      pkh user_hash NOT NULL REFERENCES tezos_user(hash),
      nonce bytea NOT NULL,
      PRIMARY KEY (hash, nonce))|};

    {|CREATE INDEX faucet_hash ON faucet (hash)|};

    {|CREATE TABLE transaction (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      source user_hash NOT NULL REFERENCES tezos_user(hash),
      destination user_hash NOT NULL REFERENCES tezos_user(hash),
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      amount bigint NOT NULL,
      parameters bytea,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      burn_tez bigint NOT NULL DEFAULT 0,
      collect_fee_gas bigint)|};

    {|CREATE INDEX transaction_hash_index ON transaction (hash)|};
    {|CREATE INDEX transaction_index_source ON transaction (source)|};
    {|CREATE INDEX transaction_index_destination ON transaction (destination)|};

    {|CREATE TABLE origination (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      source user_hash NOT NULL REFERENCES tezos_user(hash),
      tz1 user_hash NOT NULL REFERENCES tezos_user(hash),
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      manager user_hash NOT NULL REFERENCES tezos_user(hash),
      delegate user_hash REFERENCES tezos_user(hash),
      script_code bytea,
      script_storage_type varchar,
      script_code_hash varchar,
      spendable boolean NOT NULL,
      delegatable boolean NOT NULL,
      balance bigint NOT NULL,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      burn_tez bigint NOT NULL DEFAULT 257000)|};

    {|CREATE INDEX origination_hash_index ON origination (hash)|};
    {|CREATE INDEX origination_index_source ON origination (source)|};
    {|CREATE INDEX origination_index_tz1 ON origination (tz1)|};
    {|CREATE INDEX origination_index_delegate ON origination (delegate)|};
    {|CREATE INDEX origination_index_manager ON origination (manager)|};

    {|CREATE TABLE delegation (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      source hash NOT NULL,
      pubkey hash,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      delegate hash,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL)|};

    {|CREATE INDEX delegation_hash_index ON delegation (hash)|};
    {|CREATE INDEX delegation_index_source ON delegation (source)|};
    {|CREATE INDEX delegation_index_delegate ON delegation (delegate)|};

    {|CREATE TABLE endorsement (
      hash operation_hash PRIMARY KEY REFERENCES operation(hash),
      source hash NOT NULL,
      edpk hash,
      block_hash hash NOT NULL,
      slot bigint[] NOT NULL,
      block_level bigint)|};

    {|CREATE INDEX endorsement_block ON endorsement (block_hash)|};
    {|CREATE INDEX endorsement_index_source ON endorsement (source)|};
    {|CREATE INDEX endorsement_index_block_level ON endorsement (block_level)|};

    {|CREATE TABLE proposal (
      hash operation_hash PRIMARY KEY REFERENCES operation(hash),
      source hash NOT NULL,
      voting_period int NOT NULL,
      proposals text[] NOT NULL)|};

    {|CREATE TYPE ballot_vote AS ENUM ('Yay', 'Nay', 'Pass')|};

    {|CREATE TABLE ballot (
      hash operation_hash PRIMARY KEY REFERENCES operation(hash),
      source hash NOT NULL,
      voting_period int NOT NULL,
      proposal hash NOT NULL,
      ballot text NOT NULL)|};


    {|CREATE TABLE marketcap (
      currency_id VARCHAR NOT NULL,
      name VARCHAR NOT NULL,
      symbol VARCHAR NOT NULL,
      rank VARCHAR NOT NULL,
      price_usd VARCHAR NOT NULL,
      price_btc VARCHAR NOT NULL,
      volume_usd_24 VARCHAR,
      market_cap_usd VARCHAR,
      available_supply VARCHAR,
      total_supply VARCHAR,
      max_supply VARCHAR,
      percent_change_1 VARCHAR,
      percent_change_24 VARCHAR,
      percent_change_7 VARCHAR,
      last_updated bigint PRIMARY KEY,
      valid boolean NOT NULL DEFAULT true)|};

    {|CREATE TABLE peers (
      id bigserial,
      peer_id varchar PRIMARY KEY,
      country_name VARCHAR NOT NULL,
      country_code VARCHAR NOT NULL,
      point_id VARCHAR,
      trusted boolean NOT NULL,
      score float NOT NULL,
      state VARCHAR NOT NULL,
      total_sent bigint NOT NULL,
      total_received bigint NOT NULL,
      current_inflow bigint NOT NULL,
      current_outflow bigint NOT NULL,
      last_failed_connection_point VARCHAR,
      last_failed_connection_date VARCHAR,
      last_rejected_connection_point VARCHAR,
      last_rejected_connection_date VARCHAR,
      last_established_connection_point VARCHAR,
      last_established_connection_date VARCHAR,
      last_disconnection_point VARCHAR,
      last_disconnection_date VARCHAR,
      last_seen_point VARCHAR,
      last_seen_date VARCHAR,
      last_miss_point VARCHAR,
      last_miss_date VARCHAR )|};

    {|CREATE TABLE switch (
      cycle bigint PRIMARY KEY,
      switch_count bigint NOT NULL,
      longest_alt_chain bigint NOT NULL)|};

    {|CREATE TABLE level_rights (
      level bigint PRIMARY KEY,
      bakers user_hashes NOT NULL,
      endorsers user_hashes NOT NULL,
      bakers_priority int[] NOT NULL,
      cycle bigint NOT NULL,
      slots int[] NOT NULL,
      ready boolean NOT NULL DEFAULT false)|};

    {|CREATE INDEX level_rights_level_index ON level_rights (level)|};
    {|CREATE INDEX level_rights_endorsers_index ON level_rights (endorsers)|};
    {|CREATE INDEX level_rights_cycle_index ON level_rights (cycle)|};
    {|CREATE INDEX level_rights_bakers_index ON level_rights (bakers)|};

    {|CREATE TABLE header (
      id bigserial PRIMARY KEY,
      level bigint NOT NULL,
      proto bigint NOT NULL,
      predecessor block_hash NOT NULL REFERENCES block (hash) DEFERRABLE INITIALLY DEFERRED,
      timestamp timestamp NOT NULL,
      validation_pass bigint NOT NULL,
      operations_hash hash NOT NULL,
      fitness hash NOT NULL,
      context VARCHAR NOT NULL,
      priority integer NOT NULL DEFAULT 0,
      commited_nonce_hash hash NOT NULL,
      pow_nonce hash NOT NULL,
      signature VARCHAR NOT NULL)|};

    {|CREATE TABLE double_baking_evidence (
      hash operation_hash PRIMARY KEY REFERENCES operation(hash),
      header1 int REFERENCES header(id),
      header2 int REFERENCES header(id),
      accused varchar,
      denouncer varchar,
      lost_deposit bigint,
      lost_rewards bigint,
      lost_fees bigint,
      gain_rewards bigint)|};

    {|CREATE TABLE double_endorsement_evidence (
      hash operation_hash PRIMARY KEY REFERENCES operation(hash),
      block_hash1 block_hash NOT NULL,
      level1 bigint NOT NULL,
      block_hash2 block_hash NOT NULL,
      level2 bigint NOT NULL,
      accused varchar NOT NULL,
      denouncer varchar NOT NULL,
      lost_deposit bigint NOT NULL,
      lost_rewards bigint NOT NULL,
      lost_fees bigint NOT NULL,
      gain_rewards bigint NOT NULL)|};

    {|CREATE TABLE reveal (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      source hash NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      pubkey hash,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL)|};

    {|CREATE INDEX reveal_hash_index ON reveal (hash)|};
    {|CREATE INDEX reveal_index_source ON reveal (source)|};

    {|CREATE TABLE snapshot_rolls (
      id bigserial NOT NULL UNIQUE,
      cycle bigint PRIMARY KEY,
      index int NOT NULL,
      rolls_count int NOT NULL,
      gas_limit bigint,
      storage_limit bigint,
      ready boolean NOT NULL DEFAULT false)|};

    {|CREATE UNIQUE INDEX snapshot_rolls_id_index ON snapshot_rolls (id)|};
    {|CREATE UNIQUE INDEX snapshot_rolls_cycle_index ON snapshot_rolls (cycle)|};

    {|CREATE TABLE snapshot_owner (
      id bigint REFERENCES snapshot_rolls(id) ON DELETE CASCADE NOT NULL,
      tz1 user_hash NOT NULL,
      count int NOT NULL,
      staking_balance bigint NOT NULL,
      delegated_balance bigint NOT NULL)|};

    {|CREATE INDEX snapshot_owner_id_index ON snapshot_owner (id)|};
    {|CREATE INDEX snapshot_owner_tz1_index ON snapshot_owner (tz1)|};

    {|CREATE TABLE snapshot_deleguees (
      id bigint REFERENCES snapshot_rolls(id) ON DELETE CASCADE NOT NULL,
      tz1 user_hash NOT NULL,
      deleguee user_hash NOT NULL,
      balance bigint NOT NULL)|};

    {|CREATE INDEX snapshot_deleguees_id_index ON snapshot_deleguees (id)|};
    {|CREATE INDEX snapshot_deleguees_tz1_index ON snapshot_deleguees (tz1)|};

    {|CREATE TABLE activation (
      hash operation_hash NOT NULL REFERENCES operation(hash),
      pkh hash NOT NULL,
      secret VARCHAR NOT NULL,
      PRIMARY KEY (hash, pkh))|};

    {|CREATE UNIQUE INDEX activation_hahs_index ON activation (hash)|};
    {|CREATE UNIQUE INDEX activation_pkh_index ON activation (pkh)|};

    {|CREATE TABLE activation_balance (
      hash operation_hash NOT NULL REFERENCES operation(hash) PRIMARY KEY,
      pkh hash NOT NULL,
      balance bigint NOT NULL)|};

    {|CREATE UNIQUE INDEX activation_balance_hahs_index ON activation_balance (hash)|};
    {|CREATE UNIQUE INDEX activation_balance_pkh_index ON activation_balance (pkh)|};

    {|CREATE TABLE crawler_activity (
      name varchar PRIMARY KEY,
      timestamp double precision NOT NULL,
      delay int NOT NULL)|};

    {|CREATE UNIQUE INDEX crawler_activity_name_index ON crawler_activity (name) ;|};

    {|CREATE TABLE user_alias (
      tz varchar PRIMARY KEY,
      alias varchar(42) NOT NULL)|};

    {|CREATE UNIQUE INDEX user_alias_index ON user_alias (tz)|};

    {|CREATE TABLE authorized (hash hash PRIMARY KEY)|};

    {|CREATE TABLE endorsement_all (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      block_hash varchar NOT NULL,
      block_level bigint NOT NULL,
      priority int NOT NULL,
      slots int[] NOT NULL,
      op_block_hash varchar NOT NULL,
      op_level bigint NOT NULL,
      op_cycle bigint NOT NULL,
      distance_level int NOT NULL DEFAULT -1,
      network varchar NOT NULL,
      timestamp timestamp NOT NULL,
      PRIMARY KEY (hash, op_block_hash))|};

    {|CREATE TABLE endorsement_last (
      id bigserial NOT NULL,
      hash varchar NOT NULL,
      source varchar NOT NULL,
      block_hash varchar NOT NULL,
      block_level bigint NOT NULL,
      priority int NOT NULL,
      slots int[] NOT NULL,
      op_block_hash varchar NOT NULL,
      op_level bigint NOT NULL,
      op_cycle bigint NOT NULL,
      distance_level int NOT NULL DEFAULT -1,
      network varchar NOT NULL,
      timestamp timestamp NOT NULL,
      PRIMARY KEY (hash, op_block_hash))|};

    {|CREATE INDEX block_level_endorsement_all_index ON endorsement_all (block_level)|};
    {|CREATE INDEX op_level_endorsement_all_index ON endorsement_all (op_level)|};
    {|CREATE INDEX op_cycle_endorsement_all_index ON endorsement_all (op_cycle)|};
    {|CREATE INDEX source_endorsement_all_index ON endorsement_all (source)|};
    {|CREATE INDEX op_block_hash_endorsement_all_index ON endorsement_all (op_block_hash)|};
    {|CREATE INDEX distance_level_endorsement_all_index ON endorsement_all (distance_level)|};
    {|CREATE INDEX timestamp_endorsement_all_index ON endorsement_all (timestamp)|};
    {|CREATE INDEX block_level_endorsement_last_index ON endorsement_last (block_level)|};
    {|CREATE INDEX op_level_endorsement_last_index ON endorsement_last (op_level)|};
    {|CREATE INDEX op_cycle_endorsement_last_index ON endorsement_last (op_cycle)|};
    {|CREATE INDEX source_endorsement_last_index ON endorsement_last (source)|};
    {|CREATE INDEX op_block_hash_endorsement_last_index ON endorsement_last (op_block_hash)|};
    {|CREATE INDEX distance_level_endorsement_last_index ON endorsement_last (distance_level)|};
    {|CREATE INDEX timestamp_endorsement_last_index ON endorsement_last (timestamp)|};

    {|CREATE TABLE transaction_all (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      destination varchar NOT NULL,
      fee bigint NOT NULL DEFAULT 0,
      counter bigint NOT NULL,
      amount bigint NOT NULL DEFAULT 0,
      parameters bytea,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      timestamp_op timestamp NOT NULL,
      burn_tez bigint NOT NULL DEFAULT 0,
      collect_fee_gas bigint,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE TABLE transaction_last (
      id bigserial NOT NULL,
      hash varchar NOT NULL,
      source varchar NOT NULL,
      destination varchar NOT NULL,
      fee bigint NOT NULL DEFAULT 0,
      counter bigint NOT NULL,
      amount bigint NOT NULL DEFAULT 0,
      parameters bytea DEFAULT '',
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      timestamp_op timestamp NOT NULL,
      burn_tez bigint NOT NULL DEFAULT 0,
      collect_fee_gas bigint,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE INDEX source_transaction_all_index ON transaction_all (source)|};
    {|CREATE INDEX destination_transaction_all_index ON transaction_all (destination)|};
    {|CREATE INDEX op_level_transaction_all_index ON transaction_all (op_level)|};
    {|CREATE INDEX op_block_hash_transaction_all_index ON transaction_all (op_block_hash)|};
    {|CREATE INDEX distance_level_transaction_all_index ON transaction_all (distance_level)|};
    {|CREATE INDEX timestamp_transaction_all_index ON transaction_all (timestamp_op)|};
    {|CREATE INDEX source_transaction_last_index ON transaction_last (source)|};
    {|CREATE INDEX destination_transaction_last_index ON transaction_last (destination)|};
    {|CREATE INDEX op_level_transaction_last_index ON transaction_last (op_level)|};
    {|CREATE INDEX op_block_hash_transaction_last_index ON transaction_last (op_block_hash)|};
    {|CREATE INDEX distance_level_transaction_last_index ON transaction_last (distance_level)|};
    {|CREATE INDEX timestamp_transaction_last_index ON transaction_last (timestamp_op)|};

    {|CREATE TABLE cycle_count (
      cycle bigint PRIMARY KEY,
      nb_transaction bigint NOT NULL DEFAULT 0,
      nb_delegation bigint NOT NULL DEFAULT 0,
      nb_origination bigint NOT NULL DEFAULT 0,
      nb_activation bigint NOT NULL DEFAULT 0,
      nb_reveal bigint NOT NULL DEFAULT 0,
      nb_dbe bigint NOT NULL DEFAULT 0,
      nb_dee bigint NOT NULL DEFAULT 0,
      nb_nonce bigint NOT NULL DEFAULT 0,
      nb_endorsement bigint[] NOT NULL DEFAULT '{}',
      nb_endorsement_op bigint NOT NULL DEFAULT 0,
      nb_prio bigint[] NOT NULL DEFAULT '{}')|};

    {|CREATE INDEX cycle_count_index ON cycle_count (cycle)|};

    {|CREATE TABLE cycle_count_baker (
      cycle bigint NOT NULL,
      tz varchar NOT NULL,
      nb_baking bigint[] NOT NULL DEFAULT '{}',
      nb_miss_baking bigint NOT NULL DEFAULT 0,
      nb_alt_baking bigint NOT NULL DEFAULT 0,
      nb_endorsement bigint[] NOT NULL DEFAULT '{}',
      nb_miss_endorsement bigint NOT NULL DEFAULT 0,
      nb_alt_endorsement bigint NOT NULL DEFAULT 0,
      fees bigint NOT NULL DEFAULT 0,
      time float NOT NULL DEFAULT 0.,
      PRIMARY KEY (cycle, tz))|};

    {|CREATE INDEX cycle_count_baker_index ON cycle_count_baker (cycle)|};
    {|CREATE INDEX cycle_count_tz_index ON cycle_count_baker (tz)|};

    {|CREATE TABLE operation_count_user (
      tz varchar PRIMARY KEY,
      nb_transaction_src bigint NOT NULL DEFAULT 0,
      nb_transaction_dst bigint NOT NULL DEFAULT 0,
      nb_delegation_src bigint NOT NULL DEFAULT 0,
      nb_delegation_dlg bigint NOT NULL DEFAULT 0,
      nb_origination_src bigint NOT NULL DEFAULT 0,
      nb_origination_man bigint NOT NULL DEFAULT 0,
      nb_origination_tz1 bigint NOT NULL DEFAULT 0,
      nb_origination_dlg bigint NOT NULL DEFAULT 0,
      nb_activation bigint NOT NULL DEFAULT 0,
      nb_reveal bigint NOT NULL DEFAULT 0,
      nb_dbe_bk bigint NOT NULL DEFAULT 0,
      nb_dbe_acc bigint NOT NULL DEFAULT 0,
      nb_nonce bigint NOT NULL DEFAULT 0,
      nb_endorsement bigint NOT NULL DEFAULT 0,
      nb_dee_dn bigint NOT NULL DEFAULT 0,
      nb_dee_acc bigint NOT NULL DEFAULT 0)|};

    {|CREATE INDEX operation_count_user_index ON operation_count_user (tz)|};

    {|CREATE OR REPLACE FUNCTION incr_agg_prio(array_prio bigint[], prio int,
      nslot int, cond bool)
      RETURNS BIGINT[] AS $$
      BEGIN
      IF prio >= 0 AND cond THEN
      FOR i IN 1 .. prio LOOP array_prio[i] := coalesce(array_prio[i], 0); END LOOP;
      array_prio[prio+1] := COALESCE(array_prio[prio+1],0::bigint) + COALESCE(nslot::bigint, 0::bigint);
      END IF;
      RETURN COALESCE(array_prio, '{}');
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE OR REPLACE FUNCTION fagg_prio(array_prio bigint[])
      RETURNS BIGINT[] AS $$
      BEGIN
      RETURN COALESCE(ARRAY_REPLACE(array_prio, NULL, 0::bigint), '{}');
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE AGGREGATE agg_prio(prio int, nslot int, cond bool) (
      INIT_COND='{}',
      STYPE=BIGINT[],
      SFUNC=incr_agg_prio,
      FINALFUNC=fagg_prio);|};

    {|CREATE OR REPLACE FUNCTION array_lin(array1 bigint[], array2 bigint[],
      factor1 bigint, factor2 bigint)
      RETURNS BIGINT[] AS $$
      BEGIN
      FOR i IN 1 .. COALESCE(GREATEST(array_length(array1,1),array_length(array2,1))
      ,0) LOOP
      array1[i] := COALESCE(factor1, 1) * COALESCE(array1[i],0) +
      COALESCE(factor2, 1) * COALESCE(array2[i],0);
      END LOOP;
      RETURN COALESCE(array1,'{}');
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE OR REPLACE FUNCTION array_sum(arrhh bigint[], start int)
      RETURNS BIGINT AS $$
      DECLARE s bigint := 0;
      BEGIN
      IF arrhh <> '{}' THEN
      FOR i IN start .. COALESCE(array_upper(arrhh,1),0) LOOP
      s := s + COALESCE(arrhh[i], 0);
      END LOOP;
      END IF;
      RETURN s;
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE OR REPLACE FUNCTION array_wavg(arrhh bigint[], start int)
      RETURNS double precision AS $$
      DECLARE s1 double precision := 0.;
      DECLARE s2 double precision := 0;
      BEGIN
      FOR i IN start .. COALESCE(array_length(arrhh,1),0) LOOP
      s2 := s2 + cast( arrhh[i] as double precision);
      s1 := s1 + cast((i-1) * arrhh[i] as double precision);
      END LOOP;
      IF s2 <> 0 THEN
      s1 := s1 / s2;
      ELSE
      s1 := 0;
      END IF;
      RETURN s1;
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE OR REPLACE FUNCTION init_array_prio(priority int, nslot bigint)
      RETURNS BIGINT[] AS $$
      DECLARE arrhhh BIGINT[] := '{}';
      BEGIN
      FOR i IN 1 .. priority LOOP arrhhh[i] := 0; END LOOP;
      arrhhh[priority + 1] := COALESCE(nslot,0);
      RETURN arrhhh;
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE TABLE count_info (
      hash varchar NOT NULL,
      level bigint NOT NULL,
      info varchar PRIMARY KEY)|};

    {|CREATE TABLE day_context (
      id bigserial PRIMARY KEY,
      day timestamp NOT NULL UNIQUE)|};

    {|CREATE INDEX day_context_index ON day_context (day)|};

    {|CREATE TABLE context_totals (
      id bigint REFERENCES day_context(id),
      hash varchar,
      period varchar,
      period_kind varchar,
      addresses int,
      keys int ,
      revealed int ,
      originated int ,
      contracts int ,
      roll_owners int ,
      rolls int ,
      delegated bigint ,
      delegators int ,
      deleguees int ,
      self_delegates int ,
      multi_deleguees int ,
      current_balances bigint ,
      full_balances bigint ,
      staking_balances bigint ,
      frozen_balances bigint ,
      frozen_deposits bigint ,
      frozen_rewards bigint ,
      frozen_fees bigint ,
      paid_bytes bigint ,
      used_bytes bigint)|};

    {|CREATE TABLE top_balances (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      balance bigint NOT NULL)|};

    {|CREATE TABLE top_frozen_balances (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      frozen_balance bigint NOT NULL)|};

    {|CREATE TABLE top_frozen_deposits (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      frozen_deposits bigint NOT NULL)|};

    {|CREATE TABLE top_frozen_rewards (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      frozen_rewards bigint NOT NULL)|};

    {|CREATE TABLE top_paid_bytes (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      paid_bytes bigint NOT NULL)|};

    {|CREATE TABLE top_staking_balances (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      staking_balance bigint NOT NULL)|};

    {|CREATE TABLE top_total_balances (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      total_balance bigint NOT NULL)|};

    {|CREATE TABLE top_total_delegated (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      total_delegated bigint NOT NULL)|};

    {|CREATE TABLE top_total_delegators (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      total_delegators bigint NOT NULL)|};

    {|CREATE TABLE top_total_frozen_fees (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      total_frozen_fees bigint NOT NULL)|};

    {|CREATE TABLE top_used_bytes (
      id bigint REFERENCES day_context(id),
      hash varchar NOT NULL,
      used_bytes bigint NOT NULL)|};

    {|CREATE TABLE balance_updates(
      id SERIAL PRIMARY KEY,
      hash user_hash NOT NULL,
      block_hash block_hash NOT NULL,
      diff bigint NOT NULL,
      date timestamp NOT NULL,
      update_type VARCHAR NOT NULL,
      operation_type VARCHAR NOT NULL,
      internal BOOLEAN NOT NULL,
      level int NOT NULL,
      frozen BOOLEAN NOT NULL,
      burn BOOLEAN NOT NULL,
      distance_level int NOT NULL DEFAULT -1)|};

    {|CREATE INDEX bu_level ON balance_updates (level)|};
    {|CREATE INDEX bu_hash ON balance_updates (hash)|};

    {|CREATE TABLE balance_from_balance_updates(
      id SERIAL PRIMARY KEY,
      hash user_hash NOT NULL,
      spendable_balance bigint NOT NULL,
      frozen bigint NOT NULL,
      rewards bigint NOT NULL,
      fees bigint NOT NULL,
      deposits bigint NOT NULL,
      cycle int NOT NULL,
      CONSTRAINT balance_hash_cycle_pair UNIQUE (hash,cycle))|};

    {|CREATE INDEX balances_index_spendable ON balance_from_balance_updates(spendable_balance)|};
    {|CREATE INDEX balances_index_hash ON balance_from_balance_updates(hash)|};
    {|CREATE INDEX balances_index_cycle ON balance_from_balance_updates(cycle)|};

    {|CREATE TABLE coingecko_exchange (
      name varchar NOT NULL,
      base varchar NOT NULL,
      target varchar NOT NULL,
      timestamp varchar NOT NULL,
      volume float NOT NULL,
      conversion float NOT NULL,
      price_usd float NOT NULL)|};

    {|CREATE TABLE origination_all (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      tz1 varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      manager varchar NOT NULL,
      delegate varchar,
      script_code bytea,
      script_storage_type varchar,
      script_code_hash varchar,
      spendable boolean NOT NULL,
      delegatable boolean NOT NULL,
      balance bigint NOT NULL,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      burn_tez bigint NOT NULL DEFAULT 0,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE TABLE origination_last (
      id bigserial NOT NULL,
      hash varchar NOT NULL,
      source varchar NOT NULL,
      tz1 varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      manager varchar NOT NULL,
      delegate varchar,
      script_code bytea,
      script_storage_type varchar,
      script_code_hash varchar,
      spendable boolean NOT NULL,
      delegatable boolean NOT NULL,
      balance bigint NOT NULL,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      burn_tez bigint NOT NULL DEFAULT 0,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE INDEX source_origination_all_index ON origination_all (source)|};
    {|CREATE INDEX tz1_origination_all_index ON origination_all (tz1)|};
    {|CREATE INDEX manager_origination_all_index ON origination_all (manager)|};
    {|CREATE INDEX delegate_origination_all_index ON origination_all (delegate)|};
    {|CREATE INDEX op_level_origination_all_index ON origination_all (op_level)|};
    {|CREATE INDEX op_block_hash_origination_all_index ON origination_all (op_block_hash)|};
    {|CREATE INDEX distance_level_origination_all_index ON origination_all (distance_level)|};
    {|CREATE INDEX timestamp_origination_all_index ON origination_all (timestamp_op)|};
    {|CREATE INDEX source_origination_last_index ON origination_last (source)|};
    {|CREATE INDEX tz1_origination_last_index ON origination_last (tz1)|};
    {|CREATE INDEX manager_origination_last_index ON origination_last (manager)|};
    {|CREATE INDEX delegate_origination_last_index ON origination_last (delegate)|};
    {|CREATE INDEX op_level_origination_last_index ON origination_last (op_level)|};
    {|CREATE INDEX op_block_hash_origination_last_index ON origination_last (op_block_hash)|};
    {|CREATE INDEX distance_level_origination_last_index ON origination_last (distance_level)|};
    {|CREATE INDEX timestamp_origination_last_index ON origination_last (timestamp_op)|};

    {|CREATE TABLE delegation_all (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      delegate varchar,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE TABLE delegation_last (
      id bigserial NOT NULL,
      hash varchar NOT NULL,
      source varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      delegate varchar,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE INDEX source_delegation_all_index ON delegation_all (source)|};
    {|CREATE INDEX delegate_delegation_all_index ON delegation_all (delegate)|};
    {|CREATE INDEX op_level_delegation_all_index ON delegation_all (op_level)|};
    {|CREATE INDEX op_block_hash_delegation_all_index ON delegation_all (op_block_hash)|};
    {|CREATE INDEX distance_level_delegation_all_index ON delegation_all (distance_level)|};
    {|CREATE INDEX timestamp_delegation_all_index ON delegation_all (timestamp_op)|};
    {|CREATE INDEX source_delegation_last_index ON delegation_last (source)|};
    {|CREATE INDEX delegate_delegation_last_index ON delegation_last (delegate)|};
    {|CREATE INDEX op_level_delegation_last_index ON delegation_last (op_level)|};
    {|CREATE INDEX op_block_hash_delegation_last_index ON delegation_last (op_block_hash)|};
    {|CREATE INDEX distance_level_delegation_last_index ON delegation_last (distance_level)|};
    {|CREATE INDEX timestamp_delegation_last_index ON delegation_last (timestamp_op)|};

    {|CREATE TABLE activation_all (
      hash varchar NOT NULL,
      pkh varchar NOT NULL,
      secret varchar NOT NULL,
      balance bigint DEFAULT 0,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp)|};

    {|CREATE TABLE activation_last (
      id bigserial NOT NULL,
      hash varchar NOT NULL,
      pkh varchar NOT NULL,
      secret varchar NOT NULL,
      balance bigint DEFAULT 0,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp)|};

    {|CREATE INDEX pkh_activation_all_index ON activation_all (pkh)|};
    {|CREATE INDEX op_level_activation_all_index ON activation_all (op_level)|};
    {|CREATE INDEX op_block_hash_activation_all_index ON activation_all (op_block_hash)|};
    {|CREATE INDEX distance_level_activation_all_index ON activation_all (distance_level)|};
    {|CREATE INDEX timestamp_activation_all_index ON activation_all (timestamp_op)|};
    {|CREATE INDEX pkh_activation_last_index ON activation_last (pkh)|};
    {|CREATE INDEX op_level_activation_last_index ON activation_last (op_level)|};
    {|CREATE INDEX op_block_hash_activation_last_index ON activation_last (op_block_hash)|};
    {|CREATE INDEX distance_level_activation_last_index ON activation_last (distance_level)|};
    {|CREATE INDEX timestamp_activation_last_index ON activation_last (timestamp_op)|};

    {|CREATE TABLE reveal_all (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      pubkey varchar,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE TABLE reveal_last (
      id bigserial NOT NULL,
      hash varchar NOT NULL,
      source varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      pubkey varchar,
      gas_limit bigint,
      storage_limit bigint,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE INDEX source_reveal_all_index ON reveal_all (source)|};
    {|CREATE INDEX pubkey_reveal_all_index ON reveal_all (pubkey)|};
    {|CREATE INDEX op_level_reveal_all_index ON reveal_all (op_level)|};
    {|CREATE INDEX op_block_hash_reveal_all_index ON reveal_all (op_block_hash)|};
    {|CREATE INDEX distance_level_reveal_all_index ON reveal_all (distance_level)|};
    {|CREATE INDEX timestamp_reveal_all_index ON reveal_all (timestamp_op)|};
    {|CREATE INDEX source_reveal_last_index ON reveal_last (source)|};
    {|CREATE INDEX pubkey_reveal_last_index ON reveal_last (pubkey)|};
    {|CREATE INDEX op_level_reveal_last_index ON reveal_last (op_level)|};
    {|CREATE INDEX op_block_hash_reveal_last_index ON reveal_last (op_block_hash)|};
    {|CREATE INDEX distance_level_reveal_last_index ON reveal_last (distance_level)|};
    {|CREATE INDEX timestamp_reveal_last_index ON reveal_last (timestamp_op)|};

    {|CREATE TABLE snapshot_voting_rolls (
      voting_period int NOT NULL,
      delegate varchar NOT NULL,
      rolls int NOT NULL,
      ready bool NOT NULL DEFAULT false,
      PRIMARY KEY (voting_period, delegate))|};

    {|CREATE TABLE quorum (
      voting_period int PRIMARY KEY,
      value int NOT NULL,
      voting_period_kind varchar NOT NULL)|};

    {|CREATE TABLE services(
      name varchar NOT NULL,
      tz1 varchar NOT NULL,
      logos varchar[] NOT NULL,
      url varchar NOT NULL DEFAULT '',
      description varchar(1000),
      kind varchar NOT NULL,
      sponsored timestamp,
      page varchar,
      delegations_page boolean NOT NULL DEFAULT true,
      account_page boolean NOT NULL DEFAULT true,
      addresses varchar[],
      aliases varchar[],
      display_delegation BOOLEAN NOT NULL DEFAULT true,
      display_account BOOLEAN NOT NULL DEFAULT true,
      PRIMARY KEY (name, kind))|};

    {|CREATE TABLE balance_snapshot(
      hash user_hash PRIMARY KEY,
      spendable_balance bigint NOT NULL,
      frozen bigint NOT NULL,
      rewards bigint NOT NULL,
      fees bigint NOT NULL,
      deposits bigint NOT NULL,
      modified BOOLEAN NOT NULL)|};

    {|CREATE TABLE seed_nonce_revelation_all (
      hash varchar NOT NULL,
      level bigint NOT NULL,
      nonce varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      baker varchar)|};

    {|CREATE INDEX level_revelation_all_index ON seed_nonce_revelation_all (level)|};

    {|CREATE TABLE protocol_constants(
      level_start bigint PRIMARY KEY,
      level_end bigint,
      protocol varchar,
      block_hash varchar NOT NULL,
      distance_level int NOT NULL DEFAULT -1,
      proof_of_work_nonce_size int NOT NULL,
      nonce_length int NOT NULL,
      max_revelations_per_block int NOT NULL,
      max_operation_data_length int NOT NULL,
      preserved_cycles int NOT NULL,
      blocks_per_cycle int NOT NULL,
      blocks_per_commitment int NOT NULL,
      blocks_per_roll_snapshot int NOT NULL,
      blocks_per_voting_period int NOT NULL,
      time_between_blocks int[] NOT NULL DEFAULT '{}',
      endorsers_per_block int NOT NULL,
      hard_gas_limit_per_operation bigint NOT NULL,
      hard_gas_limit_per_block bigint NOT NULL,
      proof_of_work_threshold bigint NOT NULL,
      tokens_per_roll bigint NOT NULL,
      michelson_maximum_type_size int NOT NULL,
      seed_nonce_revelation_tip bigint NOT NULL,
      origination_burn bigint NOT NULL,
      block_security_deposit bigint NOT NULL,
      endorsement_security_deposit bigint NOT NULL,
      block_reward bigint NOT NULL,
      endorsement_reward bigint NOT NULL,
      cost_per_byte bigint NOT NULL,
      hard_storage_limit_per_operation bigint NOT NULL,
      max_proposals_per_delegate int,
      test_chain_duration bigint,
      hard_gas_limit_to_pay_fees bigint NOT NULL,
      max_operation_ttl int NOT NULL,
      protocol_revision int)|};

    {|CREATE INDEX level_start_constant_index ON protocol_constants (level_start)|};
    {|CREATE INDEX level_end_constant_index ON protocol_constants (level_end)|};

    {|CREATE OR REPLACE FUNCTION bake_time(timediff interval, priority int,
      time_between_blocks int[])
      RETURNS FLOAT AS $$
      BEGIN
      RETURN
      EXTRACT(MILLISECONDS FROM
      CASE WHEN priority < 2 THEN
      (timediff - priority * (time_between_blocks[1]::varchar)::interval)/1000.
      ELSE
      (timediff - (priority - 1) * (coalesce(time_between_blocks[2],time_between_blocks[1])::varchar)::interval - (time_between_blocks[1]::varchar)::interval)/1000.
      END);
      END;
      $$ LANGUAGE plpgsql; |};

    {|CREATE TABLE account_info(
      hash VARCHAR PRIMARY KEY,
      delegate VARCHAR,
      manager VARCHAR NOT NULL,
      spendable boolean NOT NULL DEFAULT true,
      delegatable boolean NOT NULL DEFAULT false,
      origination VARCHAR)|};

    {|CREATE INDEX hash_account_info_index ON account_info (hash)|};
    {|CREATE INDEX delegate_account_info_index ON account_info (delegate)|};

    {|CREATE OR REPLACE FUNCTION end_rewards_array(base_reward bigint,
      nb_endorsement bigint[])
      RETURNS BIGINT AS $$
      DECLARE s BIGINT := 0;
      BEGIN
      FOR i IN 1 .. COALESCE(array_upper(nb_endorsement, 1),0) LOOP
      s := s + coalesce(nb_endorsement[i], 0) * (base_reward / i);
      END LOOP;
      RETURN s;
      END;
      $$ LANGUAGE plpgsql;|};

    {|CREATE TABLE token_balances(
      account VARCHAR NOT NULL,
      contract VARCHAR NOT NULL,
      source VARCHAR NOT NULL,
      last_modified TIMESTAMP NOT NULL,
      balance BIGINT NOT NULL DEFAULT 0,
      PRIMARY KEY (account, contract, source))|};

    {|CREATE TABLE token_operations(
      transaction VARCHAR NOT NULL,
      contract VARCHAR NOT NULL,
      kind VARCHAR NOT NULL,
      source VARCHAR NOT NULL,
      destination VARCHAR NOT NULL,
      amount BIGINT NOT NULL,
      flag VARCHAR NOT NULL,
      caller VARCHAR,
      old_amount BIGINT,
      op_level BIGINT NOT NULL,
      op_block_hash VARCHAR NOT NULL,
      counter BIGSERIAL,
      timestamp TIMESTAMP NOT NULL,
      network varchar NOT NULL,
      distance_level INT NOT NULL DEFAULT -1,
      PRIMARY KEY (transaction, op_block_hash))|};

    {|CREATE TABLE activate_protocol (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      fee bigint NOT NULL DEFAULT 0,
      counter bigint NOT NULL,
      gas_limit bigint,
      storage_limit bigint,
      level int NOT NULL,
      protocol varchar,
      parameters varchar,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE TABLE manage_accounts (
      hash varchar NOT NULL,
      source varchar NOT NULL,
      fee bigint NOT NULL DEFAULT 0,
      counter bigint NOT NULL,
      gas_limit bigint,
      storage_limit bigint,
      bytes varchar NOT NULL,
      failed boolean NOT NULL,
      internal boolean NOT NULL,
      op_level bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp_block timestamp,
      errors text[])|};

    {|CREATE TABLE cycle_limits (
      cycle bigint PRIMARY KEY,
      level_start bigint NOT NULL,
      level_end bigint NOT NULL)|};
    {|CREATE INDEX cycle_limits_cycle_index ON cycle_limits (cycle)|};
    {|CREATE INDEX cycle_limits_start_index ON cycle_limits (level_start)|};
    {|CREATE INDEX cycle_limits_end_index ON cycle_limits (level_end)|};

  ]

let downgrade_2_to_1 = [
  {|DROP TABLE double_baking_evidence_all|};
  {|DROP TABLE double_endorsement_evidence_all|};
 ]

let update_1_to_2 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_2_to_1 [
    {|CREATE TABLE double_baking_evidence_all (
      hash varchar NOT NULL,
      header1 int REFERENCES header(id),
      header2 int REFERENCES header(id),
      accused varchar,
      denouncer varchar,
      lost_deposit bigint,
      lost_rewards bigint,
      lost_fees bigint,
      gain_rewards bigint,
      op_level bigint,
      op_cycle bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp timestamp );|};
    {|CREATE TABLE double_endorsement_evidence_all (
      hash varchar NOT NULL,
      block_hash1 block_hash NOT NULL,
      level1 bigint NOT NULL,
      block_hash2 block_hash NOT NULL,
      level2 bigint NOT NULL,
      accused varchar NOT NULL,
      denouncer varchar NOT NULL,
      lost_deposit bigint NOT NULL,
      lost_rewards bigint NOT NULL,
      lost_fees bigint NOT NULL,
      gain_rewards bigint NOT NULL,
      op_level bigint,
      op_cycle bigint,
      op_block_hash varchar,
      distance_level int,
      network varchar,
      timestamp timestamp);|};
    {|CREATE INDEX op_level_dbe_all_index ON double_baking_evidence_all (op_level) ;|};
    {|CREATE INDEX op_cycle_dbe_all_index ON double_baking_evidence_all (op_cycle) ;|};
    {|CREATE INDEX op_block_hash_dbe_all_index ON double_baking_evidence_all (op_block_hash) ;|};
    {|CREATE INDEX op_level_dee_all_index ON double_endorsement_evidence_all (op_level) ;|};
    {|CREATE INDEX op_cycle_dee_all_index ON double_endorsement_evidence_all (op_cycle) ;|};
    {|CREATE INDEX op_block_hash_dee_all_index ON double_endorsement_evidence_all (op_block_hash) ;|};
  ]

let downgrade_3_to_2 = [
  {|ALTER TABLE transaction_all DROP COLUMN collect_pk|};
  {|ALTER TABLE transaction_last DROP COLUMN collect_pk|};
  {|ALTER TABLE transaction DROP COLUMN collect_pk|};
 ]

let update_2_to_3 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_3_to_2 [
    {|ALTER TABLE transaction_all ADD COLUMN collect_pk varchar|};
    {|ALTER TABLE transaction_last ADD COLUMN collect_pk varchar|};
    {|ALTER TABLE transaction ADD COLUMN collect_pk varchar|};
  ]

let downgrade_4_to_3 = [
  {|ALTER TABLE token_operations DROP COLUMN balances|};
  {|ALTER TABLE token_operations DROP COLUMN balance_accounts|};
  {|ALTER TABLE token_operations DROP COLUMN balance_sources|};
  {|ALTER TABLE token_balances DROP COLUMN last_block|}
 ]

let update_3_to_4 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_4_to_3 [
    {|ALTER TABLE token_operations ADD COLUMN balances bigint[] NOT NULL DEFAULT '{}'|};
    {|ALTER TABLE token_operations ADD COLUMN balance_accounts varchar[] NOT NULL DEFAULT '{}'|};
    {|ALTER TABLE token_operations ADD COLUMN balance_sources varchar[] NOT NULL DEFAULT '{}'|};
    {|ALTER TABLE token_operations DROP COLUMN old_amount|};
    {|ALTER TABLE token_balances ADD COLUMN last_block varchar|};
  ]

let downgrade_5_to_4 = [
  {|ALTER TABLE token_balances ALTER COLUMN balance TYPE bigint|};
  {|ALTER TABLE token_operations ALTER COLUMN amount TYPE bigint|};
  {|ALTER TABLE token_operations ALTER COLUMN balances TYPE bigint[]|};
 ]

let update_4_to_5 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_5_to_4 [
    {|ALTER TABLE token_balances ALTER COLUMN balance TYPE varchar|};
    {|ALTER TABLE token_operations ALTER COLUMN amount TYPE varchar|};
    {|ALTER TABLE token_operations ALTER COLUMN balances TYPE varchar[]|};
  ]

let downgrade_6_to_5 = [
  (* radical change *)
]

let update_5_to_6 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_6_to_5 [
    {|DROP TABLE transaction CASCADE|};
    {|DROP TABLE delegation CASCADE|};
    {|DROP TABLE reveal CASCADE|};
    {|DROP TABLE origination CASCADE|};
    {|DROP TABLE endorsement CASCADE|};
    {|DROP TABLE activation CASCADE|};
    {|DROP TABLE activation_balance CASCADE|};
    {|DROP TABLE ballot CASCADE|};
    {|DROP TABLE proposal CASCADE|};
    {|DROP TABLE seed_nonce_revelation CASCADE|};
    {|DROP TABLE double_baking_evidence CASCADE|};
    {|DROP TABLE double_endorsement_evidence CASCADE|};
    {|DROP TABLE faucet CASCADE|};
    {|DROP TABLE block_operation CASCADE|};
    {|ALTER TABLE operation DROP CONSTRAINT operation_pkey|};
    {|ALTER TABLE operation ADD COLUMN block_hash varchar NOT NULL default ''|};
    {|ALTER TABLE operation ADD COLUMN network varchar NOT NULL default ''|};
    {|ALTER TABLE transaction_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE transaction_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE transaction_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE transaction_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE transaction_all ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE transaction_last ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE transaction_last ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE transaction_last ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE transaction_last ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE transaction_last ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE origination_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE origination_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE origination_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE origination_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE origination_all ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE origination_last ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE origination_last ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE origination_last ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE origination_last ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE origination_last ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE delegation_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE delegation_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE delegation_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE delegation_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE delegation_all ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE delegation_last ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE delegation_last ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE delegation_last ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE delegation_last ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE delegation_last ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE reveal_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE reveal_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE reveal_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE reveal_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE reveal_all ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE reveal_last ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE reveal_last ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE reveal_last ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE reveal_last ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE reveal_last ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE activation_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE activation_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE activation_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE activation_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE activation_all ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE activation_last ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE activation_last ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE activation_last ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE activation_last ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE activation_last ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE seed_nonce_revelation_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE seed_nonce_revelation_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE seed_nonce_revelation_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE seed_nonce_revelation_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE seed_nonce_revelation_all ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE activate_protocol ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE activate_protocol ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE activate_protocol ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE activate_protocol ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE activate_protocol ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE manage_accounts ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE manage_accounts ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE manage_accounts ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE manage_accounts ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE manage_accounts ALTER COLUMN timestamp_block SET NOT NULL|};
    {|ALTER TABLE double_baking_evidence_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE double_baking_evidence_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE double_baking_evidence_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE double_baking_evidence_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE double_baking_evidence_all ALTER COLUMN timestamp SET NOT NULL|};
    {|ALTER TABLE double_endorsement_evidence_all ALTER COLUMN op_level SET NOT NULL|};
    {|ALTER TABLE double_endorsement_evidence_all ALTER COLUMN op_block_hash SET NOT NULL|};
    {|ALTER TABLE double_endorsement_evidence_all ALTER COLUMN distance_level SET NOT NULL|};
    {|ALTER TABLE double_endorsement_evidence_all ALTER COLUMN network SET NOT NULL|};
    {|ALTER TABLE double_endorsement_evidence_all ALTER COLUMN timestamp SET NOT NULL|};
    {|ALTER TABLE operation ALTER COLUMN op_anon_type SET NOT NULL|};
    {|ALTER TABLE operation ALTER COLUMN op_manager_type SET NOT NULL|};
    {|CREATE TABLE transaction_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      errors text[],
      source varchar NOT NULL,
      destination varchar NOT NULL,
      fee bigint NOT NULL DEFAULT 0,
      counter bigint NOT NULL,
      amount bigint NOT NULL DEFAULT 0,
      parameters bytea,
      gas_limit bigint,
      storage_limit bigint,
      collect_fee_gas bigint,
      collect_pk varchar)|};
    {|CREATE TABLE reveal_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      errors text[],
      source varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      pubkey varchar NOT NULL,
      gas_limit bigint,
      storage_limit bigint)|};
    {|CREATE TABLE delegation_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      errors text[],
      source varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      delegate varchar NOT NULL,
      gas_limit bigint,
      storage_limit bigint)|};
    {|CREATE TABLE origination_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      errors text[],
      source varchar NOT NULL,
      kt1 varchar NOT NULL,
      fee bigint NOT NULL,
      counter bigint NOT NULL,
      manager varchar NOT NULL,
      delegate varchar,
      script_code bytea,
      script_storage_type varchar,
      script_code_hash varchar,
      spendable boolean NOT NULL,
      delegatable boolean NOT NULL,
      balance bigint NOT NULL,
      gas_limit bigint,
      storage_limit bigint)|};
    {|CREATE TABLE activation_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      errors text[],
      timestamp_op timestamp NOT NULL,
      pkh varchar NOT NULL,
      secret varchar NOT NULL)|};
    {|CREATE TABLE endorsement_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      errors text[],
      block_level bigint NOT NULL,
      priority int)|};
    {|CREATE TABLE seed_nonce_revelation_pending (
      hash varchar NOT NULL,
      branch varchar NOT NULL,
      status varchar NOT NULL,
      timestamp_op timestamp NOT NULL,
      errors text[],
      level bigint NOT NULL,
      nonce varchar NOT NULL)|}
  ]

let downgrade_7_to_6 = [
  {|ALTER TABLE origination_all RENAME COLUMN kt1 TO tz1|};
  {|ALTER TABLE origination_last RENAME COLUMN kt1 TO tz1|};
  {|ALTER TABLE snapshot_owner RENAME COLUMN dn1 TO tz1|};
  {|ALTER TABLE snapshot_deleguees RENAME COLUMN dn1 TO tz1|};
  {|ALTER TABLE user_alias RENAME COLUMN dn TO tz|};
  {|ALTER TABLE cycle_count_baker RENAME COLUMN dn TO tz|};
  {|ALTER TABLE operation_count_user RENAME COLUMN dn TO tz|};
  {|ALTER TABLE operation_count_user RENAME COLUMN nb_origination_kt1 TO nb_origination_tz1|};
  {|ALTER TABLE services RENAME COLUMN dn1 TO tz1|};
  {|ALTER TABLE transaction_all RENAME COLUMN burn_dun TO burn_tez|};
  {|ALTER TABLE transaction_last RENAME COLUMN burn_dun TO burn_tez|};
  {|ALTER TABLE origination_all RENAME COLUMN burn_dun TO burn_tez|};
  {|ALTER TABLE origination_last RENAME COLUMN burn_dun TO burn_tez|};
  {|ALTER TABLE dune_user RENAME TO tezos_user|};
  {|ALTER INDEX dune_user_index_hash RENAME TO tezos_user_index|};
  {|ALTER INDEX dune_index_contract RENAME TO tezos_index_contract|};
  {|ALTER INDEX dune_user_pkey RENAME TO tezos_user_pkey|};
  {|ALTER INDEX snapshot_owner_dn1_index RENAME TO snapshot_owner_tz1_index|};
  {|ALTER INDEX snapshot_deleguees_dn1_index RENAME TO snapshot_deleguees_tz1_index|};
  {|ALTER INDEX cycle_count_dn_index RENAME TO cycle_count_tz_index|};
  {|ALTER INDEX kt1_origination_all_index RENAME TO tz1_origination_all_index|};
  {|ALTER INDEX kt1_origination_last_index RENAME TO tz1_origination_last_index|};

]

let update_6_to_7 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_7_to_6 [
    {|ALTER TABLE origination_all RENAME COLUMN tz1 TO kt1|};
    {|ALTER TABLE origination_last RENAME COLUMN tz1 TO kt1|};
    {|ALTER TABLE snapshot_owner RENAME COLUMN tz1 TO dn1|};
    {|ALTER TABLE snapshot_deleguees RENAME COLUMN tz1 TO dn1|};
    {|ALTER TABLE user_alias RENAME COLUMN tz TO dn|};
    {|ALTER TABLE cycle_count_baker RENAME COLUMN tz TO dn|};
    {|ALTER TABLE operation_count_user RENAME COLUMN tz TO dn|};
    {|ALTER TABLE operation_count_user RENAME COLUMN nb_origination_tz1 TO nb_origination_kt1|};
    {|ALTER TABLE services RENAME COLUMN tz1 TO dn1|};
    {|ALTER TABLE transaction_all RENAME COLUMN burn_tez TO burn_dun|};
    {|ALTER TABLE transaction_last RENAME COLUMN burn_tez TO burn_dun|};
    {|ALTER TABLE origination_all RENAME COLUMN burn_tez TO burn_dun|};
    {|ALTER TABLE origination_last RENAME COLUMN burn_tez TO burn_dun|};
    {|ALTER TABLE tezos_user RENAME TO dune_user|};
    {|ALTER INDEX tezos_user_index_hash RENAME TO dune_user_index|};
    {|ALTER INDEX tezos_index_contract RENAME TO dune_index_contract|};
    {|ALTER INDEX tezos_user_pkey RENAME TO dune_user_pkey|};
    {|ALTER INDEX snapshot_owner_tz1_index RENAME TO snapshot_owner_dn1_index|};
    {|ALTER INDEX snapshot_deleguees_tz1_index RENAME TO snapshot_deleguees_dn1_index|};
    {|ALTER INDEX cycle_count_tz_index RENAME TO cycle_count_dn_index|};
    {|ALTER INDEX tz1_origination_all_index RENAME TO kt1_origination_all_index|};
    {|ALTER INDEX tz1_origination_last_index RENAME TO kt1_origination_last_index|};
  ]

let upgrades = [
  0, update_0_to_1;
  1, update_1_to_2;
  2, update_2_to_3;
  3, update_3_to_4;
  4, update_4_to_5;
  5, update_5_to_6;
  6, update_6_to_7;
]

let downgrades = [
  7, downgrade_7_to_6;
  6, downgrade_6_to_5;
  5, downgrade_5_to_4;
  4, downgrade_4_to_3;
  3, downgrade_3_to_2;
  2, downgrade_2_to_1;
  1, downgrade_1_to_0;
]
