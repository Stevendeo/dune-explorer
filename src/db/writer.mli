(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types

(* DB Internals *)
val dbh : (string, bool) Hashtbl.t PGOCaml.t PGOCaml.monad
val pg_lock : (unit -> unit) -> unit

(* Writer functions *)

val head : unit -> Data_types.block option

val is_block_registered : block_hash -> bool

val block_hash : int -> block_hash

val register_dune_user : string -> unit

val register_protocol : ?distance:int64 ->
  ?constants:(int -> (Dune_types.constants -> unit) -> unit)
  -> ?block_hash:block_hash -> int
  -> protocol_hash -> unit

val register_block :
  ?constants:(int -> (Dune_types.constants -> unit) -> unit)
  -> node_block -> node_level -> int64 -> int64 -> int64 -> int64 -> unit

val register_genesis : node_block -> unit

val register_init_balance :
    string -> int64 -> Data_types.Date.t-> int -> unit

val register_operations :
  ?constants:(int -> (Dune_types.constants -> unit) -> unit)
  -> node_block -> node_operation list -> unit

val register_all :
  ?constants:(int -> (Dune_types.constants -> unit) -> unit) ->
  node_block -> node_level -> node_operation list -> unit

val register_main_chain : bool -> node_block -> unit

val register_network_stats : network_stats list -> unit

val register_crawler_activity : string -> int -> unit

val update_alias : ?verbose:bool -> account_hash -> string option -> unit
val reset_alias : Data_types.account_name -> unit

val counts_downup : int -> int -> unit

val register_cycle_count_baker : int64 -> string -> unit

val register_service : Data_types.service -> unit
val check_and_update_service : Data_types.service -> unit

val register_coingecko_exchange : Data_types.gk_ticker -> unit
val register_marketcap : Data_types.marketcap -> unit
val last_voting_period : unit -> int
val register_voting_rolls : int -> Dune_types.voting_rolls list -> unit
val register_quorum : int -> int32 -> unit

val update_originated_contract : ?origination:operation_hash ->
  account_hash -> account_hash option -> account_hash -> bool -> bool -> unit

val constants : ?limit:int -> unit -> (int * Dune_types.constants) list

val insert_bu_info : Data_types.balance_update_info -> unit
val update_balance_from_balance_updates : ?force_init:bool ->
  int32 -> string -> Data_types.balance -> unit

val update_counts : ?force:bool -> block_hash -> int64 -> unit

val has_level_rights : int -> bool
val register_cycle_limits : ?force:bool -> int64 -> int64 -> int64 -> unit

val register_pending : CalendarLib.Calendar.t -> mempool -> unit
