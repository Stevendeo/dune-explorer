let () =
  let tz = Sys.argv.(1) in
  let dn = Crypto.tz_to_dn tz in
  Printf.printf "tz = %s, dn = %s\n%!" tz dn
