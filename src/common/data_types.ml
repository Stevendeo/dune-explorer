(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

include Data_types_min

type url = { address : string ; port : int }

type config =
  { nodes : url list ; data_path : string option }

type nonces = int * (operation_hash option * int * block_hash) list

type voting_period_status =
  | VPS_passed
  | VPS_wait
  | VPS_current
  | VPS_ignored

type level = {
  lvl_level : int ;
  lvl_level_position : int ;
  lvl_cycle : int ;
  lvl_cycle_position : int ;
  lvl_voting_period : int ;
  lvl_voting_period_position : int ;
}

type account = {
  account_hash : account_name ;
  account_manager : account_name ;
  account_spendable : bool ;
  account_delegatable : bool ;
}

type baking = {
  bk_block_hash : block_hash;
  bk_baker_hash : account_name;
  bk_level : int;
  bk_cycle : int;
  bk_priority : int;
  bk_missed_priority : int option;
  bk_distance_level : int;
  bk_fees : int64;
  bk_bktime : int;
  bk_baked : bool;
  bk_tsp: string
}

type baking_endorsement = {
  ebk_block : block_hash option;
  ebk_source : account_name option;
  ebk_level : int;
  ebk_cycle : int option;
  ebk_priority : int option;
  ebk_dist : int option;
  ebk_slots: int list option;
  ebk_lr_nslot: int;
  ebk_tsp: string option
}

type bk_count = {
  cnt_all: int64;
  cnt_miss: int64;
  cnt_steal: int64
}

type bk_dun = {
  dun_fee: int64;
  dun_reward: int64;
  dun_deposit: int64
}

type cycle_baking = {
  cbk_cycle : int;
  cbk_depth : int;
  cbk_count : bk_count;
  cbk_dun : bk_dun;
  cbk_priority : float option;
  cbk_bktime : int option;
}

type cycle_endorsement = {
  ced_cycle : int;
  ced_depth : int;
  ced_slots : bk_count;
  ced_dun : bk_dun;
  ced_priority : float;
}

type baker_rights = {
  br_level : int;
  br_cycle : int;
  br_priority : int;
  br_depth : int;
}

type endorser_rights = {
  er_level : int;
  er_cycle : int;
  er_nslot : int;
  er_depth : int;
}

type cycle_rights = {
  cr_cycle : int;
  cr_nblocks : int;
  cr_priority : float; (* used for number of slots for endorsements *)
}

type rights = {
  r_level : int;
  r_bakers : account_name list;
  r_endorsers : account_name list;
  r_bakers_priority : int list;
  r_baked : (account_name * int) option
}

type marketcap = {
  mc_id : string ;
  name : string ;
  symbol : string ;
  rank : string ;
  price_usd : string ;
  price_btc : string ;
  volume_usd_24 : string option ;
  market_cap_usd : string option ;
  available_supply : string option ;
  total_supply : string option ;
  max_supply : string option ;
  percent_change_1 : string option ;
  percent_change_24 : string option ;
  percent_change_7 : string option ;
  last_updated : string ;
}

type country_stats = {
  country_name : string ;
  country_code : string ;
  total : int ;
}

type baker_stats = {
  baker_hash : account_name ;
  nb_blocks : int ;
  volume_total : int64 ;
  fees_total : int64 ;
  nb_endorsements : int
}

type 'a per_day = {
  pd_days : string array;
  pd_value : 'a array;
}

type service_stats = {
  service_name : string;
  service_ok_days_nb : int array;
  service_ok_days_dt : float array;
  service_fail_days_nb : int array;
  service_fail_days_dt : float array;
}

type timing_stats = {
  timing_uptime : string;
  timing_period : string;
  timing_services : service_stats array;
}

type mini_stats = {
  ms_period : string array;
  ms_nhours : int array;
  ms_nblocks : int array;
  ms_nops : int array;
  ms_volume : int64 array;
  ms_fees : int64 array;
}

type health_stats = {
  cycle_start_level : int ;
  cycle_end_level : int ;
  cycle_volume : int64 ;
  cycle_fees : int64 ;
  cycle_bakers : int ;
  cycle_endorsers : int ;
  cycle_date_start : (int * int * int) ;
  cycle_date_end : (int * int * int) ;
  endorsements_rate : float ;
  main_endorsements_rate : float ;
  alt_endorsements_rate : float ;
  empty_endorsements_rate : float ;
  double_endorsements : int ;
  main_revelation_rate : float ;
  alternative_heads_number : int ;
  switch_number : int ;
  longest_switch_depth : int ;
  mean_priority : float ;
  score_priority : float ;
  biggest_block_volume : string * int ;
  biggest_block_fees : string * int ;
  top_baker : account_name;
}

type account_bonds_rewards = {
  acc_b_rewards : int64 ;
  acc_b_deposits : int64 ;
  acc_fees : int64 ;
  acc_e_rewards : int64 ;
  acc_e_deposits : int64
}

type account_extra_rewards ={
  acc_dnb_gain: int64;
  acc_dnb_deposit: int64;
  acc_dnb_rewards: int64;
  acc_dnb_fees: int64;
  acc_dne_gain: int64;
  acc_dne_deposit: int64;
  acc_dne_rewards: int64;
  acc_dne_fees: int64;
  acc_rv_rewards: int64;
  acc_rv_lost_rewards: int64;
  acc_rv_lost_fees:int64
}

type account_status = {
  account_status_hash : account_name ;
  account_status_revelation : operation_hash option ;
  account_status_origination : operation_hash option ;
}

type delegate_service = {
  del_srv_dn1 : account_name option ;
  del_srv_name : string ;
  del_srv_descr : string ;
  del_srv_url : string ;
  del_srv_logo : string ; (* file name, relative to "/images/" *)
  del_srv_logo2 : string option ; (* file name, relative to "/images/" *)
}

type supply_info = {
  foundation : int64 ;
  early_bakers : int64 ;
  contributors : int64 ;
  unfrozen_rewards : int64 ;
  missing_revelations : int ;
  revelation_rewards : int64 ;
  burned_dun_revelation : int64 ;
  burned_dun_origination : int64 ;
  burned_dun_double_baking : int64 ;
  burned_dun_double_endorsement : int64 ;
  total_supply_ico : int64 ;
  current_circulating_supply : int64 ;
}

type balance_break_down = {
  h_activated_balance : int64 ;
  h_unfrozen_rewards : int64 ;
  h_revelation_rewards : int64 ;
  h_missing_revelations : int ;
  h_burned_dun_revelation : int64 ;
  h_burned_dun_origination : int64 ;
  h_dun_origination_recv : int64 ;
  h_dun_origination_send : int64 ;
  h_burned_dun_transaction : int64 ;
  h_dun_transaction_recv : int64 ;
  h_dun_transaction_send : int64 ;
  h_burned_dun_double_baking : int64 ;
  h_burned_dun_double_endorsement : int64 ;
  h_dun_dbe_rewards : int64 ;
  h_total : int64 ;
}

type service = {
  srv_kind : string; (* "wallet" or "delegate" or "dunscan" *)
  srv_dn1 : string option;
  srv_name : string;
  srv_url : string;
  srv_logo : string; (* file name, related to /images/ *)
  srv_logo2 : string option; (* file name, related to /images/ *)
  srv_logo_payout : string option; (* file name, related to /images/ *)
  srv_descr : string option;
  srv_sponsored : string option;
  srv_page : string option;
  srv_delegations_page : bool ;
  srv_account_page : bool ;
  srv_aliases: account_name list option ;
  srv_display_delegation_page : bool ;
  srv_display_account_page : bool ;
}

type crawler_activity = {
  crawler_name : string ;
  crawler_timestamp : float ;
  crawler_delay : int ;
}

type rewards_status =
  | Rewards_pending
  | Rewards_delivered
  | Cycle_pending
  | Cycle_in_progress

type rewards_split = {
  rs_delegate_staking_balance : int64 ;
  rs_delegators_nb : int ;
  rs_delegators_balance : (account_name * int64) list ;
  rs_block_rewards : int64 ;
  rs_fees : int64 ;
  rs_endorsement_rewards : int64 ;
  rs_baking_rights_rewards : int64 ;
  rs_endorsing_rights_rewards : int64 ;
  rs_gain_from_denounciation_b : int64 ;
  rs_lost_deposit_b : int64 ;
  rs_lost_rewards_b : int64 ;
  rs_lost_fees_b : int64 ;
  rs_gain_from_denounciation_e : int64 ;
  rs_lost_deposit_e : int64 ;
  rs_lost_rewards_e : int64 ;
  rs_lost_fees_e : int64 ;
  rs_rv_rewards : int64 ;
  rs_rv_lost_rewards : int64 ;
  rs_rv_lost_fees : int64
}

type all_rewards_split = {
  ars_cycle : int ;
  ars_delegate_staking_balance : int64 ;
  ars_delegators_nb : int ;
  ars_delegate_delegated_balance : int64 ;
  ars_block_rewards : int64 ;
  ars_fees : int64 ;
  ars_endorsement_rewards : int64 ;
  ars_baking_rights_rewards : int64 ;
  ars_endorsing_rights_rewards : int64 ;
  ars_status : rewards_status ;
  ars_gain_from_denounciation_b : int64 ;
  ars_lost_deposit_b : int64 ;
  ars_lost_rewards_b : int64 ;
  ars_lost_fees_b : int64 ;
  ars_gain_from_denounciation_e : int64 ;
  ars_lost_deposit_e : int64 ;
  ars_lost_rewards_e : int64 ;
  ars_lost_fees_e : int64 ;
  ars_rv_rewards : int64 ;
  ars_rv_lost_rewards : int64 ;
  ars_rv_lost_fees : int64
}

type delegator_reward = {
  dor_cycle: int;
  dor_delegate: account_name;
  dor_staking_balance: int64;
  dor_balance: int64;
  dor_rewards: int64;
  dor_extra_rewards: int64;
  dor_losses: int64;
  dor_status: rewards_status
}

type delegator_reward_details = {
  dor_block_rewards: int64;
  dor_end_rewards: int64;
  dor_fees: int64;
  dor_rv_rewards: int64;
  dor_dnb_gain: int64;
  dor_dne_gain: int64;
  dor_rv_lost_rewards: int64;
  dor_rv_lost_fees: int64;
  dor_dnb_lost_deposit: int64;
  dor_dnb_lost_rewards: int64;
  dor_dnb_lost_fees: int64;
  dor_dne_lost_deposit: int64;
  dor_dne_lost_rewards: int64;
  dor_dne_lost_fees: int64;
}

type snapshot = {
  snap_cycle : int ;
  snap_index : int ;
  snap_level : int ;
  snap_rolls : int
}

type proto_details = {
  prt_index: int;
  prt_hash: protocol_hash;
  prt_start: int;
  prt_end: int option;
  prt_activate: string option;
}

type h24_stats = {
  h24_end_rate : float ;
  h24_block_0_rate : float ;
  h24_transactions : int ;
  h24_originations : int ;
  h24_delegations : int ;
  h24_activations : int ;
  h24_baking_rate : float ;
  h24_active_baker : int ;
}

type balance_update_info =
  {bu_account : account_hash;
   bu_block_hash : block_hash;
   bu_diff : int64;
   bu_date : Date.t;
   bu_update_type : string;
   bu_op_type : string;
   bu_internal : bool;
   bu_frozen : bool;
   bu_level : int;
   bu_cycle : int option;
   mutable bu_burn : bool
  }

type balance = {
  b_spendable :  int64;
  b_frozen :  int64;
  b_rewards :  int64;
  b_fees :  int64;
  b_deposits :  int64;
}

type context_top_kind =
  | TBalances
  | TFrozen_balances
  | TFrozen_deposits
  | TFrozen_rewards
  | TPaid_bytes
  | TStaking_balances
  | TTotal_balances
  | TTotal_delegated
  | TTotal_delegators
  | TTotal_frozen_fees
  | TUsed_bytes

(* top accounts at the beginning of this period *)
type context_top_accounts = {
  context_top_period : string;
  context_top_kind : string;
  context_top_hash : string;
  context_top_list : (string * int64) list ;
}

(* top accounts at the beginning of this period *)
type top_accounts = {
  top_period : string;
  top_kind : string;
  top_hash : string;
  top_list : (account_name * int64) list ;
}

type context_file_with_diff = {
  context_level : level option ;
  context_addresses : int;
  context_addresses_diff : float ;
  context_keys : int ;
  context_keys_diff : float ;
  context_revealed : int ;
  context_revealed_diff : float ;
  context_originated : int ;
  context_originated_diff : float ;
  context_contracts : int ;
  context_contracts_diff : float ;
  context_roll_owners : int ;
  context_roll_owners_diff : float ;
  context_rolls : int ;
  context_rolls_diff : float ;
  context_delegated : int64 ;
  context_delegated_diff : float ;
  context_delegators : int ;
  context_delegators_diff : float ;
  context_deleguees : int ;
  context_deleguees_diff : float ;
  context_self_delegates : int ;
  context_self_delegates_diff : float ;
  context_multi_deleguees : int ;
  context_multi_deleguees_diff : float ;
  context_current_balances : int64 ;
  context_current_balances_diff : float ;
  context_full_balances : int64 ;
  context_full_balances_diff : float ;
  context_staking_balances : int64 ;
  context_staking_balances_diff : float ;
  context_frozen_balances : int64 ;
  context_frozen_balances_diff : float ;
  context_frozen_deposits : int64 ;
  context_frozen_deposits_diff : float ;
  context_frozen_rewards : int64 ;
  context_frozen_rewards_diff : float ;
  context_frozen_fees : int64 ;
  context_frozen_fees_diff : float ;
  context_paid_bytes : int64 ;
  context_paid_bytes_diff : float ;
  context_used_bytes : int64 ;
  context_used_bytes_diff : float ;
}

type ico_constants = {
  ico_foundation_tokens : int64 ;
  ico_early_tokens : int64 ;
  ico_contributors_tokens : int64 ;
  ico_remaining_fundraiser : int64 ;
  ico_wallets : int ;
}

type api_server_config = {
  conf_network : string ;
  mutable conf_constants : ( int * Dune_types.constants ) list ; (* cycle x constants *)
  conf_ico : ico_constants ;
  conf_has_delegation : bool ;
  conf_has_marketcap : bool ;
}

type versions = {
  server_version : string;
  server_build : string;
  server_commit : string;
}

type api_server_info = {
  mutable api_config : api_server_config ;
  mutable api_date : float ;
  mutable api_versions : versions ;
}

type www_server_info = {
  mutable www_currency_name : string ;
  mutable www_currency_short : string ;
  mutable www_currency_symbol : string list ;
  mutable www_languages : (string * string) list ;
  mutable www_apis : string array ;
  mutable www_auth : string option ;
  mutable www_logo : string ;
  mutable www_footer : string ;
  mutable www_networks : ( string * string ) list ; (* name x www_url *)
  mutable www_themes : (string * string) list ;
  mutable www_recaptcha_key : string option ;
  mutable www_csv_server : (string * string) option ;
  mutable www_charts_server : string option ;
}

type gk_coin = {gk_usd: float; gk_btc: float}
type gk_market_data = {
  gk_price: gk_coin;
  gk_market_volume: gk_coin;
  gk_1h: gk_coin;
  gk_24h: gk_coin;
  gk_7d: gk_coin
}
type gk_ticker = {
  gk_last: float;
  gk_target: string;
  gk_tsp: string;
  gk_anomaly: bool;
  gk_converted_last: gk_coin;
  gk_volume: float;
  gk_stale: bool;
  gk_base: string;
  gk_converted_volume: gk_coin;
  gk_market: string
}
type gk_shell = {
  gk_last_updated: string;
  gk_market_data: gk_market_data;
  gk_tickers: gk_ticker list
}
type ex_ticker = {
  ex_base: string;
  ex_target: string;
  ex_volume: float;
  ex_conversion: float;
  ex_price_usd: float;
  ex_tsp: string;
}
type exchange_info = {
  ex_name: string;
  ex_total_volume: float;
  ex_tickers: ex_ticker list
}

type proposal = {
  prop_period: int;
  prop_period_kind: Dune_types.voting_period_kind;
  prop_hash: proposal_hash;
  prop_count: int;
  prop_votes: int;
  prop_source: account_name;
  prop_op: operation_hash option;
  prop_ballot: ballot_type option
}

type period_kind_summary =
  | Sum_proposal of (protocol_hash * int * float)
  | Sum_proposal_empty
  (* hash * Nb proposals * % winner*)
  | Sum_testing_vote of (float * float * float)
  (* turn_out * expected_quorum * supermajority *)
  | Sum_testing
  | Sum_promo of (float * float * float)
  (* turn_out * expected_quorum * supermajority *)

type period_summary = {
  sum_period : int ;
  sum_cycle : int ;
  sum_level : int ;
  sum_period_info : period_kind_summary ;
}

(* pending operations *)
type pending_info = {
  pe_hash : operation_hash;
  pe_branch : block_hash;
  pe_status : string;
  pe_tsp : string;
  pe_errors : op_error list option;
}
type pending_manager = {
  pe_src : account_name;
  pe_counter : int64;
  pe_fee : int64;
  pe_gas_limit : Z.t option;
  pe_storage_limit : Z.t option;
}
type pending_seed_nonce_revelation = {
  pe_seed_info : pending_info;
  pe_seed_level : int ;
  pe_seed_nonce : string ;
}
type pending_activation = {
  pe_act_info : pending_info;
  pe_act_pkh : account_name;
  pe_act_secret : string;
}
type pending_endorsement = {
  pe_end_info : pending_info;
  pe_end_level : int;
  pe_end_prio : int option;
}
type pending_transaction = {
  pe_tr_info : pending_info;
  pe_tr_man : pending_manager;
  pe_tr_dst : account_name;
  pe_tr_amount : int64;
  pe_tr_parameters : string option;
  pe_tr_collect_fee_gas : int64 option;
  pe_tr_collect_pk : string option;
}
type pending_origination = {
  pe_or_info : pending_info;
  pe_or_man : pending_manager;
  pe_or_manager : account_name;
  pe_or_delegate : account_name option;
  pe_or_script : script option;
  pe_or_spendable : bool;
  pe_or_delegatable : bool;
  pe_or_balance : int64;
  pe_or_kt1 : account_name;
}
type pending_delegation = {
  pe_del_info : pending_info;
  pe_del_man : pending_manager;
  pe_del_delegate : account_name;
}
type pending_reveal = {
  pe_rvl_info : pending_info;
  pe_rvl_man : pending_manager;
  pe_rvl_pubkey : string;
}
type pending_operation =
  | PSeed_nonce_revelation of pending_seed_nonce_revelation
  | PActivation of pending_activation
  | PEndorsement of pending_endorsement
  | PTransaction of pending_transaction
  | POrigination of pending_origination
  | PDelegation of pending_delegation
  | PReveal of pending_reveal
