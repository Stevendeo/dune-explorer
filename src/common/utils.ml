(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types

let pending_block_hash = "prevalidation"

let orphan_block_hash = "Orphan"

let debug flag fmt =
  if flag then Printf.eprintf fmt
  else Printf.ifprintf stderr fmt

let unopt_i64_slot =
  List.map (function
      | None -> assert false
      | Some s -> Int64.to_int s)


let opt_list list = List.map (fun elt -> Some elt) list

let get_op_from_double_endorsement_evidence = function
  | Anonymous _ | Tokened _ -> assert false
  | Sourced op ->
    begin match op with
      | Endorsement op -> op
      | _ -> assert false
    end

let string_of_op_type = function
  | Anonymous _ -> "Anonymous"
  | Tokened _ -> "Tokened"
  | Sourced op ->
    begin match op with
      | Endorsement _ -> "Endorsement"
      | Amendment (_ , op) ->
        begin match op with
          | Proposal _ -> "Proposals"
          | Ballot _ -> "Ballot"
        end
      | Manager (_, _, _) -> "Manager"
      | Dictator op ->
        begin match op with
          | Activate -> "Activate"
          | Activate_testnet -> "Activate_testnet"
        end
    end

let string_of_manager_op_type = function
  | Sourced Manager (_, _, list) ->
    (if List.exists (function Transaction _ -> true | _ -> false) list then
       [ "Transaction" ]
     else []) @
    (if List.exists (function Origination _ -> true | _ -> false) list then
       [ "Origination" ]
     else []) @
    (if List.exists (function Reveal _ -> true | _ -> false) list then
       [ "Reveal" ]
     else []) @
    (if List.exists (function Delegation _ -> true | _ -> false) list then
       [ "Delegation" ]
     else [])
  | _ -> []

let string_of_anonymous_op_type = function
  | Anonymous list ->
    (if List.exists (function
         | Seed_nonce_revelation _ -> true
         | _ -> false) list then
       [ "Nonce" ]
     else []) @
    (if List.exists (function Activation _ -> true | _ -> false) list then
       [ "Activation" ]
     else []) @
    (if List.exists (function
         | Double_endorsement_evidence _ -> true
         | _ -> false) list then
       [ "Double_endorsement_evidence" ]
     else []) @
    (if List.exists (function
         | Double_baking_evidence _ -> true
         | _ -> false) list then
       [ "Double_baking_evidence" ]
     else [])
  | _ -> []

let json_root = function
  | `O ctns -> `O ctns
  | `A ctns -> `A ctns
  | `Null -> `O []
  | oth -> `A [ oth ]

let split_ymd_timestamp date_str =
  let l = String.split_on_char '-' date_str in
  match l with
  | [ year; month ; day ] ->
    int_of_string year,
    int_of_string month,
    int_of_string day
  | _ -> assert false

let voting_period_of_summary = function
  | Sum_proposal _ -> "Proposal"
  | Sum_proposal_empty -> "Proposal"
  | Sum_testing_vote _ -> "Exploration"
  | Sum_testing -> "Testing"
  | Sum_promo _ -> "Promotion"

let pp_voting_period_status = function
  | VPS_passed -> "VP passed"
  | VPS_wait -> "VP wait (future)"
  | VPS_current -> "VP current"
  | VPS_ignored -> "VP never happened"

let string_of_ballot_vote = function
  | Yay -> "Yay"
  | Nay -> "Nay"
  | Pass -> "Pass"

let ballot_of_string = function
  | "Yay" | "yay" -> Yay
  | "Pass"| "pass" -> Pass
  | "Nay" | "nay" -> Nay
  | _ -> Nay

let rec script_expr_tr = function
  | Dune_types.Int z -> Data_types.Int z
  | Dune_types.String s -> Data_types.String s
  | Dune_types.Bytes b -> Data_types.Bytes b
  | Dune_types.Prim (s, se, sl) ->
    Data_types.Prim (s, List.map script_expr_tr se, sl)
  | Dune_types.Seq se -> Data_types.Seq (List.map script_expr_tr se)

let int_of_ballot_vote rolls = function
  | Yay -> rolls
  | Nay -> -rolls
  | Pass -> 0

let ballot_of_int = function
  | i when i > 0 -> Yay
  | i when i < 0 -> Nay
  | _ -> Pass

let dune_to_data_date d =
  Data_types.Date.from_string @@ Dune_types.Date.to_string d
