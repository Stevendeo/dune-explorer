let () =
  let dn = Sys.argv.(1) in
  let tz = Crypto.dn_to_tz dn in
  Printf.printf "dn = %s, tz = %s\n%!" dn tz
