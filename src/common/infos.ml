(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types

let dune_constants = ref []

type net =
  | Testnet
  | Mainnet

let net = match DunscanConfig.database with
  | "mainnet" | "betanet" | "dune_mainnet" -> Mainnet
  | _ -> Testnet

let dun_units = 1_000_000L

let versions = {
  server_version = DunscanConfig.version;
  server_build = DunscanConfig.en_date;
  server_commit = DunscanConfig.commit;
}

let mainnet = {
  ico_foundation_tokens = 36_270_000_000_000L ;
  ico_early_tokens = 128_960_000_000_000L ;
  ico_contributors_tokens = 641_900_718_270_862L ;
  ico_remaining_fundraiser = 85_540_002_008_097L;
  ico_wallets = 0 ;
}

(*
let alphanet = {
  ico_company_tokens = 0L ;
  ico_foundation_tokens = 32_000_000_000000L ;
  ico_early_tokens = 0L ;
  ico_contributors_tokens = 760_000_000_000000L ;
  ico_wallets = 30_000 ;
}

let zeronet = {
  ico_company_tokens = 0L ;
  ico_foundation_tokens = 8_040_000_000000L ;
  ico_early_tokens = 0L ;
  ico_contributors_tokens = 760_000_000_000000L ;
  ico_wallets = 30_000 ;
}
*)

let api =
  let api_config =
    {
      conf_network = "Mainnet" ;
      conf_constants = [];
      conf_ico = mainnet;
      conf_has_delegation = false ;
      conf_has_marketcap = false ;
    }
  in
  let api_date = 0. in
  let api_versions  = {
    server_version = "--" ;
    server_build = "--" ;
    server_commit = "--" ;
  }
  in
  {
    api_config ;
    api_date ;
    api_versions ;
  }


let www = {
  www_currency_name = "DUNE" ;
  www_currency_short = "DUN" ;
  www_currency_symbol =  [ "#273" ] ;
  www_languages = [ "English", "en" ; "Français", "fr" ] ;
  www_apis = [||] ;
  www_auth = None ;
  www_logo = "dunscan-logo.png" ;
  www_footer = "/footer.html" ;
  www_networks = [] ;
  www_themes = [ "Light", "default"; "Dark", "slate" ] ;
  www_recaptcha_key = None;
  www_csv_server = None;
  www_charts_server = None ;
}

let save_api_config filename =
  let oc = open_out filename in
  output_string oc (EzEncoding.construct ~compact:false
                      Api_encoding.V1.Server.api_server_config
                      api.api_config);
  close_out oc

let init_constants csts =
  (* constants input should be in increasing order *)
  let csts = List.fold_left (fun acc (level_start, cst) -> match acc with
      | (_, c) :: _ when c = cst -> acc
      | acc -> (level_start, cst) :: acc) [] csts in
  dune_constants := csts;
  api.api_config.conf_constants <- !dune_constants

let add_constants level_start cst =
  let rec iter = function
    | (lstart, co) :: t when lstart > level_start -> (lstart, co) :: (iter t)
    | (lstart, co) :: t when lstart = level_start || co = cst -> (lstart, co) :: t
    | l -> (level_start, cst) :: l in
  dune_constants := iter !dune_constants;
  api.api_config.conf_constants <- !dune_constants

let constants ~level =
  let rec iter = function
    | [] -> assert false
    | (lstart , c) :: _ when level >= lstart -> c
    | _ :: t -> iter t in
  iter !dune_constants

let last_constants () =
  snd @@ List.hd !dune_constants

let cycle_constants ~cycle =
  let rec start_level = function
    | 0 -> 1
    | n ->
      let start_level = start_level (n-1) in
      let cst = constants ~level:start_level in
      start_level + cst.Dune_types.blocks_per_cycle in
  constants ~level:(start_level cycle)
