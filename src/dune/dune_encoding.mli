(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types

val z_encoding : Z.t Json_encoding.encoding

module Micheline : sig
  val encode : script_expr_t -> string
  val decode : string -> script_expr_t
  val script_expr_encoding : script_expr_t Json_encoding.encoding
  val script_expr_str_encoding : string Json_encoding.encoding
  val script_encoding : (script_expr_t option * script_expr_t * string option) Json_encoding.encoding
  val script_str_encoding : script Json_encoding.encoding
end
module Level : sig
  val encoding : node_level Json_encoding.encoding
end
module Time : sig
  val encoding : string Json_encoding.encoding
end
module Contracts : sig
  val encoding : string list Json_encoding.encoding
end
module Header : sig
  val shell_encoding : block_shell Json_encoding.encoding
  val encoding : block_header Json_encoding.encoding
end
module Block : sig
  val encoding : node_block Json_encoding.encoding
  val block_metadata_encoding : block_metadata Json_encoding.encoding
  val genesis_encoding : node_block Json_encoding.encoding
end
module Operation : sig
  val encoding : node_operation list list Json_encoding.encoding
end
module Balance_updates : sig
  val balance_encoding :
    balance_updates Json_encoding.encoding
  val encoding :
    balance_updates list Json_encoding.encoding
end
module Voting_period_repr : sig
  val kind_encoding : voting_period_kind Json_encoding.encoding
end
module Account : sig
  val encoding : node_account Json_encoding.encoding
end
module Baking_rights : sig
  val encoding : baking_rights list Json_encoding.encoding
end
module Endorsing_rights : sig
  val encoding : endorsing_rights list Json_encoding.encoding
end
module Network : sig
  val encoding : network_stats list Json_encoding.encoding
  val versions : network_version Json_encoding.encoding
  val data_stats : data_stats Json_encoding.encoding
end
module Rolls : sig
  val rolls_encoding : (int * int * rolls) Json_encoding.encoding
  val deleguees : string list Json_encoding.encoding
  val delegation_balances : (string * int64) list Json_encoding.encoding
end
module Predecessor : sig
  val encoding : block_hash Json_encoding.encoding
end
module Alternative_blocks : sig
  val encoding : block_hash list list Json_encoding.encoding
end
module Mempool : sig
  val pending_operation : pending_operation Json_encoding.encoding
  val encoding : mempool Json_encoding.encoding
end
module Delegate : sig
  val encoding : delegate_details Json_encoding.encoding
  val delegated_contracts_encoding : (string list) Json_encoding.encoding
end
module Injection : sig
  val injection_encoding : (string * string * bool) Json_encoding.encoding
  val result_encoding : injection_result Json_encoding.encoding
end
module Votes : sig
  val voting_rolls_encodings : voting_rolls list Json_encoding.encoding
end
val constants : constants Json_encoding.encoding
val conn_metadata_encoding :
  conn_metadata Json_encoding.encoding

module Checkpoint : sig
  val block_shell_gen_encoding : block_shell_gen Json_encoding.encoding
  val checkpoint_encoding : checkpoint Json_encoding.encoding
end
