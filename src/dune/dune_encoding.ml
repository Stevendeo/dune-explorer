(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Json_encoding
open Dune_types

include Dune_encoding_min

let float = Json_encoding.float

let conn_metadata_encoding =
  conv
    (fun { disable_mempool ; private_node } ->
       (disable_mempool, private_node) )
    (fun
      (disable_mempool, private_node) ->
      { disable_mempool ; private_node })
    (obj2
       (req "disable_mempool" bool)
       (req "private_node" bool))

module Mempool = struct

  let pending_operation =
    conv
      (fun {pending_hash; pending_branch; pending_contents; pending_signature;
            pending_errors}
        -> (pending_hash, pending_branch, pending_contents, pending_signature,
            pending_errors))
      (fun (pending_hash, pending_branch, pending_contents, pending_signature,
            pending_errors)
        -> {pending_hash; pending_branch; pending_contents; pending_signature;
            pending_errors})
      (obj5
         (req "hash" string)
         (req "branch" string)
         (req "contents" Operation.contents_and_result_list_encoding)
         (req "signature" string)
         (opt "error" (list Error.encoding)))

  let pending_operations = list pending_operation
  let encoding =
    conv
      (fun {applied; refused; branch_refused; branch_delayed; unprocessed}
        -> (applied, refused, branch_refused, branch_delayed, unprocessed))
      (fun (applied, refused, branch_refused, branch_delayed, unprocessed)
        -> {applied; refused; branch_refused; branch_delayed; unprocessed})
      (obj5
         (req "applied" pending_operations)
         (req "refused" pending_operations)
         (req "branch_refused" pending_operations)
         (req "branch_delayed" pending_operations)
         (req "unprocessed" pending_operations))

end

module Delegate = struct
  let encoding =
    conv
      (fun { delegate_balance ; delegate_frozen_balance ;
             delegate_staking_balance ;
             delegate_delegated_contracts ;
             delegate_delegated_balance ;
             delegate_deactivated ;
             delegate_grace_period } ->
        (delegate_balance, delegate_frozen_balance,
         Json_repr.to_any `Null, delegate_staking_balance,
         delegate_delegated_contracts, delegate_delegated_balance,
         delegate_deactivated, delegate_grace_period))
      (fun (delegate_balance, delegate_frozen_balance,
            _delegate_frozen_balance_by_cycle, delegate_staking_balance,
            delegate_delegated_contracts, delegate_delegated_balance,
            delegate_deactivated, delegate_grace_period) ->
        { delegate_balance ;
          delegate_frozen_balance ;
          delegate_staking_balance ;
          delegate_delegated_contracts ;
          delegate_delegated_balance ;
          delegate_deactivated ;
          delegate_grace_period })
      (obj8
         (req "balance" int64)
         (req "frozen_balance" int64)
         (req "frozen_balance_by_cycle" any_value)
         (req "staking_balance" int64)
         (req "delegated_contracts" (list string))
         (req "delegated_balance" int64)
         (req "deactivated" bool)
         (req "grace_period" int))

  let delegated_contracts_encoding = list string

end

module Rolls = struct

  let roll_encoding =
    union [
      case
        int32
        (fun i ->
           let j = Int64.to_int32 i in
           if Int64.equal (Int64.of_int32 j) i then Some j else None)
        Int64.of_int32 ;
      case
        string
        (fun i -> Some (Int64.to_string i))
        Int64.of_string
    ]

  let rolls_encoding =
    (conv
       (fun _ -> assert false)
       (fun (c, i, rolls) ->
          c, i,
          List.map (fun (roll_owner, (roll_count, roll_change)) ->
              { roll_owner ; roll_count; roll_change }) rolls)
       (obj3
          (req "cycle" int)
          (req "index" int)
          (req "rolls"
             (list
                (tup2
                   string
                   (tup2
                      int
                      roll_encoding))))))

    (*
    let snapshot_level_encoding =
      (conv
         (fun _ -> assert false)
         (fun i -> i))
        (obj1 (req "level" int))
*)

  let deleguees =
    (conv
       (fun _ -> assert false)
       (fun deleguees -> deleguees))
      (obj1 (req "delegators" (list string)))

  let delegation_balances =
    (conv
       (fun _ -> assert false)
       (fun delegation_balances -> delegation_balances))
      (obj1 (req "balances" (list (tup2 string int64))))

end

module Baking_rights = struct

  let baking_right_encoding =
    conv
      (fun { node_br_level ; node_br_delegate ; node_br_priority ; node_br_timestamp } ->
         (node_br_level, node_br_delegate, node_br_priority, node_br_timestamp))
      (fun (node_br_level, node_br_delegate, node_br_priority, node_br_timestamp) ->
         { node_br_level ; node_br_delegate ; node_br_priority ; node_br_timestamp })
      (obj4
         (req "level" int)
         (req "delegate" string)
         (req "priority" int)
         (opt "estimated_time" string))

  let encoding = list baking_right_encoding
end

module Endorsing_rights = struct
  let endorsing_right_encoding =
    conv
      (fun { node_er_level ; node_er_delegate ; node_er_slots ; node_er_estimated_time } ->
         (node_er_level, node_er_delegate, node_er_slots, node_er_estimated_time))
      (fun (node_er_level, node_er_delegate, node_er_slots, node_er_estimated_time) ->
         { node_er_level ; node_er_delegate ; node_er_slots ; node_er_estimated_time })
      (obj4
         (req "level" int)
         (req "delegate" string)
         (req "slots" (list int))
         (opt "estimated_time" string))
  let encoding = list endorsing_right_encoding
end

module Predecessor = struct
  let encoding =
    conv
      (fun _ -> assert false)
      (fun {shell_predecessor; _} -> shell_predecessor)
      Header.shell_encoding
end

module Alternative_blocks = struct
  let encoding = list @@ list string
end

module Network = struct

  let versions1 =
    conv
      ( fun { v_name ; v_major ; v_minor } -> ( v_name , v_major , v_minor ) )
      ( fun ( v_name , v_major , v_minor ) -> { v_name ; v_major ; v_minor } )
      (obj3
         (req "name" string)
         (req "major" int)
         (req "minor" int)
      )

  let versions2 =
    conv
      ( fun { v_name ; v_major ; v_minor } -> ( v_name , v_major , v_minor ) )
      ( fun ( v_name , v_major , v_minor ) -> { v_name ; v_major ; v_minor } )
      (obj3
         (req "chain_name" string)
         (req "distributed_db_version" int)
         (req "p2p_version" int)
      )

  let versions =
    union [
      case versions2
        (fun s -> Some s)
        (fun s -> s) ;
      case versions1
        (fun _s -> None)
        (fun s -> s)
    ]

  let state_encoding =
    string_enum [
      "accepted", Accepted ;
      "running", Running ;
      "disconnected", Disconnected ;
    ]

  let id_point_encoding =
    (obj2
       (req "addr" string)
       (dft "port" int 8732))

  let time_encoding =
    (union [
        case
          string
          (fun s -> Some s)
          (fun s -> s) ;
        case
          int64
          (fun _ -> None)
          (fun i -> string_of_int @@ Int64.to_int i) ;
      ])

  let data_stats =
    conv
      (fun { total_sent ; total_recv ; current_inflow ; current_outflow } ->
         (total_sent, total_recv, current_inflow, current_outflow))
      (fun (total_sent, total_recv, current_inflow, current_outflow) ->
         { total_sent ; total_recv ; current_inflow ; current_outflow })
      (obj4
         (req "total_sent" int64)
         (req "total_recv" int64)
         (req "current_inflow" int)
         (req "current_outflow" int))

  let point_to_string = function
    | None -> None
    | Some ((addr, port), timestamp) ->
      Some (Printf.sprintf "%s:%d" addr port, timestamp)

  let encoding =
    let peer_encoding =
      conv
        (fun _ -> assert false)
        (fun (peer_id, ((score, trusted, conn_metadata, _, state, id_point, stat),
                        (last_failed_connection, last_rejected_connection,
                         last_established_connection, last_disconnection,
                         last_seen, last_miss))) ->
          let country = "", "" in (* Recomputed in the database *)
          let id_point =
            match id_point with
            | None -> None
            | Some (ip, port) -> Some (Printf.sprintf "%s:%d" ip port) in
          let last_failed_connection = point_to_string last_failed_connection in
          let last_rejected_connection = point_to_string last_rejected_connection in
          let last_established_connection = point_to_string last_established_connection in
          let last_disconnection = point_to_string last_disconnection in
          let last_seen = point_to_string last_seen in
          let last_miss = point_to_string last_miss in
          { peer_id; country; score ; trusted ; conn_metadata ;
            state ; id_point; stat ;
            last_failed_connection ; last_rejected_connection ;
            last_established_connection ; last_disconnection ;
            last_seen ; last_miss })
        (tup2
           string
           (merge_objs
              (obj7
                 (req "score" float)
                 (req "trusted" bool)
                 (opt "conn_metadata" conn_metadata_encoding)
                 (opt "peer_metadata" Json_encoding.any_value)
                 (req "state" state_encoding)
                 (opt "reachable_at" id_point_encoding)
                 (req "stat" data_stats))
              (obj6
                 (opt "last_failed_connection" ((tup2 id_point_encoding time_encoding)))
                 (opt "last_rejected_connection" ((tup2 id_point_encoding time_encoding)))
                 (opt "last_established_connection" ((tup2 id_point_encoding time_encoding)))
                 (opt "last_disconnection" ((tup2 id_point_encoding time_encoding)))
                 (opt "last_seen" ((tup2 id_point_encoding time_encoding)))
                 (opt "last_miss" ((tup2 id_point_encoding time_encoding)))))) in
    (list peer_encoding)

    (*
    let peer_to_string peer =
      match peer with
      | None -> ""
      | Some s-> s
*)

  (*    let to_peer point_id  = point_id *)

    (*
    let last_connection =
      function
      | None -> "", ""
      | Some (point, date) -> peer_to_string (Some point), date
*)
end

module Injection = struct

  let injection_encoding =
    obj3
      (req "signedOperationContents" string)
      (req "net_id" string)
      (req "force" bool)

  let result_encoding =
    let ok =
      obj1 (req "ok"
              (obj1 (req "injectedOperation" string))) in
    let outdated =
      obj3
        (req "kind" string)
        (req "id" string)
        (req "ecoproto"
           (list (obj5
                    (req "kind" string)
                    (req "id" string)
                    (req "contract" string)
                    (req "expected" int)
                    (req "found" int)
                 )))
    in
    let maybe_illformed =
      obj2
        (req "kind" string)
        (req "error" string)
    in
    let maybe_parse_error =
      obj3
        (req "kind" string)
        (req "id" string)
        (req "ecoproto"
           (list (obj2
                    (req "kind" string)
                    (req "id" string)
                 )))
    in
    let errors =
      union [
        case outdated
          (fun _ -> None)
          (fun _ -> Inj_outdated) ;
        case maybe_illformed
          (fun _ -> None)
          (fun _ -> Inj_illformed) ;
        case maybe_parse_error
          (fun _ -> None)
          (fun _ -> Inj_illformed) ;
        case any_value
          (fun _ -> None)
          (fun _s -> Inj_generic "") ;

      ]
    in
    let errors_list =
      obj1 (req "error"
              (list errors)) in
    union [
      case ok
        (fun _ -> None)
        (fun op -> Inj_ok op) ;
      case errors_list
        (fun _ -> None)
        (fun errs -> Inj_ko errs) ;
    ]
end

module Votes = struct
  let voting_rolls_encoding =
    conv
      (fun {vroll_pkh; vroll_count} -> (vroll_pkh, vroll_count))
      (fun (vroll_pkh, vroll_count) -> {vroll_pkh; vroll_count})
      (obj2
         (req "pkh" string)
         (req "rolls" int))
  let voting_rolls_encodings = list voting_rolls_encoding
end

let constants_tr =
  conv
    (fun { proof_of_work_nonce_size; nonce_length; max_revelations_per_block;
           max_operation_data_length; preserved_cycles; blocks_per_cycle;
           blocks_per_commitment; blocks_per_roll_snapshot;
           blocks_per_voting_period; time_between_blocks; endorsers_per_block;
           hard_gas_limit_per_operation; hard_gas_limit_per_block;
           proof_of_work_threshold; tokens_per_roll; michelson_maximum_type_size;
           seed_nonce_revelation_tip; origination_burn; block_security_deposit;
           endorsement_security_deposit; block_reward; endorsement_reward;
           cost_per_byte; hard_storage_limit_per_operation;
           max_proposals_per_delegate; test_chain_duration;
           hard_gas_limit_to_pay_fees; max_operation_ttl; protocol_revision}
      -> ((proof_of_work_nonce_size, nonce_length, max_revelations_per_block,
           max_operation_data_length, preserved_cycles, blocks_per_cycle,
           blocks_per_commitment, blocks_per_roll_snapshot,
           blocks_per_voting_period, time_between_blocks, endorsers_per_block,
           hard_gas_limit_per_operation, hard_gas_limit_per_block,
           proof_of_work_threshold, tokens_per_roll, michelson_maximum_type_size,
           seed_nonce_revelation_tip, Some origination_burn, block_security_deposit,
           endorsement_security_deposit, block_reward, endorsement_reward,
           cost_per_byte, hard_storage_limit_per_operation),
          (max_proposals_per_delegate, None, test_chain_duration,
           hard_gas_limit_to_pay_fees, max_operation_ttl, protocol_revision)))
    (fun ((proof_of_work_nonce_size, nonce_length, max_revelations_per_block,
           max_operation_data_length, preserved_cycles, blocks_per_cycle,
           blocks_per_commitment, blocks_per_roll_snapshot,
           blocks_per_voting_period, time_between_blocks, endorsers_per_block,
           hard_gas_limit_per_operation, hard_gas_limit_per_block,
           proof_of_work_threshold, tokens_per_roll, michelson_maximum_type_size,
           seed_nonce_revelation_tip, origination_burn, block_security_deposit,
           endorsement_security_deposit, block_reward, endorsement_reward,
           cost_per_byte, hard_storage_limit_per_operation),
          (max_proposals_per_delegate, origination_size, test_chain_duration,
           hard_gas_limit_to_pay_fees, max_operation_ttl, protocol_revision))
      ->
        let origination_burn =
          match origination_burn, origination_size with
          | Some origination_burn, _ -> origination_burn
          | _, Some origination_size -> Int64.(mul 1000L (of_int origination_size))
          | _ -> 0L in
        { proof_of_work_nonce_size; nonce_length; max_revelations_per_block;
          max_operation_data_length; preserved_cycles; blocks_per_cycle;
          blocks_per_commitment; blocks_per_roll_snapshot;
          blocks_per_voting_period; time_between_blocks; endorsers_per_block;
          hard_gas_limit_per_operation; hard_gas_limit_per_block;
          proof_of_work_threshold; tokens_per_roll; michelson_maximum_type_size;
          seed_nonce_revelation_tip; origination_burn; block_security_deposit;
          endorsement_security_deposit; block_reward; endorsement_reward;
          cost_per_byte; hard_storage_limit_per_operation;
          max_proposals_per_delegate; test_chain_duration;
          hard_gas_limit_to_pay_fees; max_operation_ttl; protocol_revision })

let constants =
  constants_tr
    (merge_objs
       (EzEncoding.obj24
          (req "proof_of_work_nonce_size" int)
          (req "nonce_length" int)
          (req "max_revelations_per_block" int)
          (req "max_operation_data_length" int)
          (req "preserved_cycles" int)
          (req "blocks_per_cycle" int)
          (req "blocks_per_commitment" int)
          (req "blocks_per_roll_snapshot" int)
          (req "blocks_per_voting_period" int)
          (req "time_between_blocks" (list int_of_string))
          (req "endorsers_per_block" int)
          (req "hard_gas_limit_per_operation" int64)
          (req "hard_gas_limit_per_block" int64)
          (req "proof_of_work_threshold" int64)
          (req "tokens_per_roll" int64)
          (req "michelson_maximum_type_size" int)
          (req "seed_nonce_revelation_tip" int64)
          (opt "origination_burn" int64)
          (req "block_security_deposit" int64)
          (req "endorsement_security_deposit" int64)
          (req "block_reward" int64)
          (req "endorsement_reward" int64)
          (req "cost_per_byte" int64)
          (req "hard_storage_limit_per_operation" int64))
       (obj6
          (opt "max_proposals_per_delegate" int)
          (opt "origination_size" int)
          (opt "test_chain_duration" int64)
          (req "hard_gas_limit_to_pay_fees" int64)
          (req "max_operation_ttl" int)
          (opt "protocol_revision" int)))

module Checkpoint = struct

  let block_shell_gen_encoding =
    conv
      (fun { shell_header ; shell_header_protocol_data } ->
         (shell_header, shell_header_protocol_data))
      (fun (shell_header, shell_header_protocol_data) ->
         { shell_header ; shell_header_protocol_data })
      (merge_objs
         Header.shell_encoding
         (obj1 (req "protocol_data" string)))

  let checkpoint_encoding =
    conv
      (fun { checkpoint_block ;
             checkpoint_point ;
             checkpoint_caboose ;
             checkpoint_history_mode } ->
        ( checkpoint_block,
          checkpoint_point,
          checkpoint_caboose,
          checkpoint_history_mode ))
      (fun ( checkpoint_block,
             checkpoint_point,
             checkpoint_caboose,
             checkpoint_history_mode ) ->
        { checkpoint_block ;
          checkpoint_point ;
          checkpoint_caboose ;
          checkpoint_history_mode }
      )
    (obj4
       (req "block" block_shell_gen_encoding)
       (req "save_point" int)
       (req "caboose" int)
       (req "history_mode" string))

end
