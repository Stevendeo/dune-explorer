(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types_min

let unopt ~default = function None -> default | Some value -> value

let string_of_ballot_vote = function
  | Yay -> "Yay"
  | Nay -> "Nay"
  | Pass -> "Pass"

let ballot_of_string = function
  | "Yay" | "yay" -> Yay
  | "Pass"| "pass" -> Pass
  | "Nay" | "nay" -> Nay
  | _ -> Nay

let voting_period_kind_of_string = function
  | "proposal" -> NProposal
  | "testing_vote" -> NTesting_vote
  | "testing" -> NTesting
  | "promotion_vote" -> NPromotion_vote
  | _ -> assert false

let string_of_voting_period_kind = function
  | NProposal -> "proposal"
  | NTesting_vote -> "testing_vote"
  | NTesting -> "testing"
  | NPromotion_vote -> "promotion_vote"

let pp_voting_period_kind = function
  | NProposal -> "Proposal"
  | NTesting_vote -> "Exploration"
  | NTesting -> "Testing"
  | NPromotion_vote -> "Promotion"

let string_of_op_contents = function
  | NSeed_nonce_revelation _ | NDouble_baking_evidence _
  | NDouble_endorsement_evidence _ | NActivation _ -> "Anonymous"
  | NEndorsement _ -> "Endorsement"
  | NProposals _ -> "Proposals"
  | NBallot _ -> "Ballot"
  | NTransaction _ | NDelegation _
  | NOrigination _ | NReveal _ -> "Manager"
  | NActivate -> "Activate"
  | NActivate_testnet -> "Activate_testnet"
  | NActivate_protocol _ -> "Activate_protocol"
  | NManage_accounts _ -> "Manage_accounts"

let string_of_anonymous_op_contents = List.fold_left (fun acc op -> match op with
    | NActivation _ -> "Activation" :: acc
    | NDouble_endorsement_evidence _ -> "Double_endorsement_evidence" :: acc
    | NDouble_baking_evidence _ -> "Double_baking_evidence" :: acc
    | NSeed_nonce_revelation _ -> "Nonce" :: acc
    | _ -> acc) []

let string_of_manager_op_contents = List.fold_left (fun acc op -> match op with
    | NTransaction _ -> "Transaction" :: acc
    | NOrigination _ -> "Origination" :: acc
    | NReveal _ -> "Reveal" :: acc
    | NDelegation _ -> "Delegation" :: acc
    | NActivate_protocol _ -> "Activate_protocol" :: acc
    | NManage_accounts _ -> "Manage_accounts" :: acc
    | _ -> acc) []

let string_of_balance_update = function
  | Contract (s, i) -> Printf.sprintf "Contract %s %Ld" s i
  | Rewards (s, i1, i2) -> Printf.sprintf "Rewards %s %d %Ld" s i1 i2
  | Fees (s, i1, i2) -> Printf.sprintf "Fees %s %d %Ld" s i1 i2
  | Deposits (s, i1, i2) -> Printf.sprintf "Deposits %s %d %Ld" s i1 i2
