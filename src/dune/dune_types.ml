(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)
include Dune_types_min

type peer = string

type pending_operation = {
  pending_hash : string;
  pending_branch : string;
  pending_contents : node_operation_type list;
  pending_signature : string;
  pending_errors : node_op_error list option;
}

type mempool = {
  applied : pending_operation list;
  refused : pending_operation list;
  branch_refused : pending_operation list;
  branch_delayed : pending_operation list;
  unprocessed : pending_operation list
}

type canonical = Canonical of script_expr_t

type delegate_details = {
  delegate_balance : int64 ;
  delegate_frozen_balance : int64 ;
  delegate_staking_balance : int64 ;
  delegate_delegated_contracts : account_hash list ;
  delegate_delegated_balance : int64 ;
  delegate_deactivated : bool ;
  delegate_grace_period : int
}

type roll = {
  roll_owner : account_hash ;
  roll_count : int ;
  roll_change : int64 ;
}

type rolls = roll list

type baking_rights = {
  node_br_level : int ;
  node_br_delegate : account_hash ;
  node_br_priority : int ;
  node_br_timestamp : string option ;
}

type endorsing_rights = {
  node_er_level : int;
  node_er_delegate : account_hash;
  node_er_slots : int list ;
  node_er_estimated_time : string option ;
}

type data_stats = {
  total_sent : int64;
  total_recv : int64;
  current_inflow : int ;
  current_outflow : int ;
}

type state = Accepted | Running | Disconnected

type conn_metadata = {
  disable_mempool : bool ;
  private_node : bool ;
}

type network_stats = {
  peer_id : string ;
  country : string * string ;   (* Country name * country code *)
  score : float ;
  trusted : bool ;
  conn_metadata : conn_metadata option ;
  state : state ;
  id_point : peer option ;
  stat : data_stats ;
  last_failed_connection : (peer * timestamp)  option ;
  last_rejected_connection : (peer * timestamp)  option ;
  last_established_connection : (peer * timestamp)  option ;
  last_disconnection : (peer * timestamp)  option ;
  last_seen : (peer * timestamp)  option ;
  last_miss : (peer * timestamp)  option ;
}

type injection_error = (* in this order *)
  | Inj_other
  | Inj_generic of string
  | Inj_outdated
  | Inj_illformed

type injection_result =
  | Inj_ok of string
  | Inj_ko of injection_error list

type constants = {
  proof_of_work_nonce_size: int;
  nonce_length: int;
  max_revelations_per_block: int;
  max_operation_data_length: int;
  preserved_cycles: int;
  blocks_per_cycle: int;
  blocks_per_commitment: int;
  blocks_per_roll_snapshot: int;
  blocks_per_voting_period: int;
  time_between_blocks: int list;
  endorsers_per_block: int;
  hard_gas_limit_per_operation: int64;
  hard_gas_limit_per_block: int64;
  proof_of_work_threshold: int64;
  tokens_per_roll: int64;
  michelson_maximum_type_size: int;
  seed_nonce_revelation_tip: int64;
  origination_burn: int64;
  block_security_deposit: int64;
  endorsement_security_deposit: int64;
  block_reward: int64;
  endorsement_reward: int64;
  cost_per_byte: int64;
  hard_storage_limit_per_operation: int64;
  max_proposals_per_delegate: int option;
  test_chain_duration: int64 option;
  hard_gas_limit_to_pay_fees: int64 ;
  max_operation_ttl: int ;
  protocol_revision : int option ;
}

type network_version = {
  v_name : string ;
  v_major : int ;
  v_minor : int ;
}

type voting_rolls = {
  vroll_pkh: account_hash;
  vroll_count: int
}

type block_shell_gen = {
  shell_header : block_shell ;
  shell_header_protocol_data : string ;
}

type checkpoint = {
  checkpoint_block : block_shell_gen ;
  checkpoint_point : int ;
  checkpoint_caboose : int ;
  checkpoint_history_mode : string ;
}
