(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

List = module("ocp-build:List", "1.0");

ocaml.has_asm = false;

OCaml.library("dunscan-utils-js", ocaml+{
 requires = [
   "ocplib-utils-js";
   "ez-api-js";
 ];
 files = [
   "lang.ml";
   "misc_js.ml", pp_js;
   "format_date.ml", pp_js;
 ];
});

OCaml.library("dunscan-common-js", ocaml+{
  requires = [
    "dunscan-utils-js";
    "js_of_ocaml-lwt";
    "glossary";
    "base58-blockies";
    "dunscan-services";
    "omd";
  ];
  files = [
    "panel.mli";
    "text.ml";
    "common.ml", pp_js;
    "tabs.ml", pp_js;
    "glossary_doc.ml", pp_js;
    "panel.ml", pp_js;
  ];
});


OCaml.library("explorer-lib", ocaml+ {
  requires = [
    "dunscan-common-js";
    "ocplib-odometer";
    "ocplib-ammap3-js";
    "dunscan-config";
    "dunscan-misc";
    "dunscan-common";
    "ocplib-recaptcha";
    "ocplib-qrcode";
  ] ;
  files = [
     "redoc.ml", pp_js;
     "dun.ml", pp_js;
     "gen.ml";
     "node_state_ui.ml", pp_js;
     "health_stats_ui.ml", pp_js;
     "context_stats_ui.ml";
     "proposals_ui.ml", pp_js;
     "home_ui.ml", pp_js;
     "search.ml", pp_js;
     "blocks_ui.ml";
     "block_ui.ml", pp_js;
     "operation_ui.ml";
     "operations_ui.ml";
     "inject_ui.ml", pp_js;
     "rolls_distribution_ui.ml", pp_js;
     "baking_charts_ui.ml", pp_js;
     "baking_ui.ml";
     "balance_charts_ui.ml", pp_js;
     "balance_ui.ml";
     "rewards_ui.ml";
     "dapps.ml";
     "top_accounts_ui.ml", pp_js;
     "accounts_ui.ml";
     "network_stats_ui.ml", pp_js;
     "charts_ui.ml", pp_js;
     "protocols_ui.ml";
     "tokens_ui.ml";
     "account_ui.ml", pp_js;
     "apps_ui.ml", pp_js;
     "api_request.ml", pp_js;
     "www_request.ml";
     "menu.ml";
     "ui.ml", pp_js;
  ];
});

OCaml.program("explorer-main", ocaml+ {
  requires = [ "explorer-lib" ] ;
  files = [ "main.ml" ];
  build_rules = [
    "%{explorer-main_FULL_DST_DIR}%/explorer-main.js", {
      build_target = true;
      sources = "%{explorer-main_FULL_DST_DIR}%/explorer-main.byte";
      commands = [
        OCaml.system([
          "js_of_ocaml";
          "+nat.js";
          "js-external-deps/BigInteger.js";
          "js-external-deps/zarith.js";
          "%{explorer-main_FULL_DST_DIR}%/explorer-main.byte";
        ])
      ]
    }
  ]
});