open Ocp_js.Html
open Common
open Bootstrap_helpers.Icon
open Bootstrap_helpers.Color
open Lang
open Text

module TransferPanel = Panel.MakePageTable(
  struct
    let title_span _ = span []
    let table_class = "default-table"
    let page_size = big_panel_number
    let name = "transfer"
    let theads () =
      tr [
        th @@ cl_icon exchange_icon (t_ s_txn_hash);
        th @@ cl_icon cube_icon (t_ s_block);
        th @@ cl_icon clock_icon (t_ s_date);
        th @@ cl_icon account_icon (t_ s_from);
        th ~a:[ a_class [ "arrow" ] ] [ txt "" ] ;
        th @@ cl_icon account_icon (t_ s_to);
        th @@ cl_icon Dun.icon (t_ s_amount);
        th @@ cl_icon bill_icon (t_ s_fee);
        th @@ cl_icon manager_icon (t_ s_caller);
      ]
  end)

let make_account_transfers symbol = function
  | [] -> [ tr [ td ~a: [ a_colspan 9 ] [ txt_t s_no_transfer ]]]
  | ops ->
    List.map (fun (op_hash, op_block_hash, level, tsp, caller, tf) ->
        let open Gen in
        let cols =
          from_transfer
            ~symbol
            ~crop_len_dn:15
            ~crop_len_hash:8
            op_block_hash op_hash level tsp caller tf in
        let arrow = td ~a:[ a_class [ blue ] ] [ right_icon () ] in
        tr [
          cols.td_tf_op_hash ;
          cols.td_tf_block_hash ;
          cols.td_tf_timestamp ;
          cols.td_tf_src ;
          arrow ;
          cols.td_tf_dst ;
          cols.td_tf_amount ;
          cols.td_tf_fee ;
          cols.td_tf_caller ;
        ]) @@ Common.get_transfers ops

module KYCPanel = Panel.MakePageTable(
  struct
    let title_span _ = span []
    let table_class = "default-table"
    let page_size = big_panel_number
    let name = "kyc"
    let theads () =
      tr [
        th @@ cl_icon exchange_icon (t_ s_txn_hash);
        th @@ cl_icon cube_icon (t_ s_block);
        th @@ cl_icon clock_icon (t_ s_date);
        th @@ cl_icon account_icon (t_ s_from);
        th @@ cl_icon params_icon (t_ s_param);
      ]
  end)

let make_account_kyc = function
  | [] -> [ tr [ td ~a: [ a_colspan 5 ] [ txt_t s_no_kyc ]]]
  | ops ->
    List.map (fun (op_hash, op_block_hash, level, tsp, caller, param) ->
        let open Gen in
        let cols =
          from_kyc
            ~crop_len_dn:15
            ~crop_len_hash:8
            op_block_hash op_hash level tsp caller param in
        tr [
          cols.td_kyc_op_hash ;
          cols.td_kyc_block_hash ;
          cols.td_kyc_timestamp ;
          cols.td_kyc_caller ;
          cols.td_kyc_param ;
        ]) @@ Common.get_kyc ops

let make_panel hash =
  match Common.token_info hash with
  | None -> div []
  | Some (flag, _) when flag = "kyc" -> div [ KYCPanel.make () ]
  | Some _ -> div [ TransferPanel.make () ]
