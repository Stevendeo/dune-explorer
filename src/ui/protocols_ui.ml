(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ocp_js.Html
open Bootstrap_helpers.Icon
open Bootstrap_helpers.Grid
open Bootstrap_helpers.Button
open Bootstrap_helpers.Form
open Dune_types
open Data_types
open Lang
open Text
open Common

module ProtocolsTable = struct
  let name = "Protocols"
  let theads () = tr [
      th @@ cl_icon (fun () -> span [txt "#"]) (t_ s_number);
      th @@ cl_icon ruler_icon (t_ s_hash);
      th @@ cl_icon (fas_u "external-link-square-alt") (t_ s_upgrade);
      th @@ cl_icon play_icon (t_ s_start);
      th @@ cl_icon stop_icon (t_ s_end);
      th @@ cl_icon (fas_u "scroll") (t_ s_code);
    ]
  let page_size = 20
  let table_class = "default-table"
end

module ProtocolsPanel =
  Panel.MakePageTable(struct
    include ProtocolsTable
    let title_span nb = Panel.title_nb s_protocols nb
  end)

let make = ProtocolsPanel.make ~footer:true

let proto_indexes = [
  "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P", 0;
  "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd", 4 ]

let to_rows =
  List.map (fun prt ->
      let elt_start = make_link @@ string_of_int prt.prt_start in
      let elt_end = match prt.prt_end with
        | None -> txt_ ()
        | Some level_end -> make_link @@ string_of_int level_end in
      let link_proto, link_activate = match prt.prt_activate with
        | None ->
          make_link ~crop_len:15 ~crop_limit:md_size
            ~args:["protocol", prt.prt_hash]
            ~path:"/constants" prt.prt_hash,
          txt_ ()
        | Some op_hash ->
          txt_ (),
          make_link ~crop_len:15 ~crop_limit:sm_size
            ~args:["level", string_of_int prt.prt_start]
            ~path:"/constants" op_hash in
      let link_code, elt_index =
        if prt.prt_index = -1 then txt_ (), txt_ ()
        else
          a ~a:[ a_href @@ Printf.sprintf
                   "https://gitlab.com/dune-network/dune-network/tree/%s/src/proto_%03d_%s/lib_protocol"
                   (String.sub DunscanConfig.database 5 ((String.length DunscanConfig.database) - 5))
                   (List.assoc prt.prt_hash proto_indexes)
                   (String.sub prt.prt_hash 0 8);
                 a_target "_blank"] [ txt "gitlab" ],
          txt @@ string_of_int prt.prt_index in
      tr [
        td [ elt_index ];
        td [ link_proto ];
        td [ link_activate ];
        td [ elt_start ];
        td [ elt_end ];
        td [ link_code ]
      ])

let update ?nrows xhr =
  ProtocolsPanel.paginate_fun to_rows ?nrows xhr


let constants_panel_id = "constants-panel"

let make_constants ?level ?protocol update =
  let title_tx = match level, protocol with
    | Some level, _ -> Printf.sprintf "Constants from level %d" level
    | _, Some protocol ->
      Printf.sprintf "Constants for protocol %s" (crop_hash ~crop_len:15 protocol)
    | _ -> "Current constants" in
  div [
    form ~a:[ a_class [form_inline; "text-right"] ] [
      div ~a:[ a_class [form_group] ] [
        label [ txt "Choose a level: " ];
        input ~a:[ a_id "constants-level-input"; a_class [form_control] ] ();
        button ~a:[
          a_class [btn; btn_primary];
          a_onclick (fun _e ->
              (try
                let level = int_of_string
                    Js_utils.(Manip.value (find_component "constants-level-input")) in
                update ?level:(Some level) ?protocol:None ()
              with _ -> Js_utils.log "cannot get the level");
              false) ] [ txt "Update" ]
      ]
    ];
    Bootstrap_helpers.Panel.make_panel
      ~panel_title_content:(span [txt title_tx])
      ~panel_class:["block-div"]
      ~panel_body_id:constants_panel_id
      ()
  ]

let update_constants (_level, cst) =
  let panel = Js_utils.find_component constants_panel_id in
  let line label fmt value =
    div ~a:[ a_class [row] ] [
      div ~a:[ a_class [csm6; clg4; clgoffset2; "text-left"; "lbl"] ] [ txt label ];
      div ~a:[ a_class [csm6; clg4; "text-right"; "value"] ] [ txtf fmt value ] ] in
  let line_option label fmt value =
    let value_tx = match value with
      | Some value -> txtf fmt value
      | None -> txt_ () in
    div ~a:[ a_class [row] ] [
      div ~a:[ a_class [csm6; clg4; clgoffset2; "text-left"; "lbl"] ] [ txt label ];
      div ~a:[ a_class [csm6; clg4; "text-right"; "value"] ] [ value_tx ] ] in
  let subtitle_class = [ "constants-subtitle" ] in
  let content = [
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Periods" ];
      line "Preserved cycles" "%d" cst.preserved_cycles;
      line "Blocks per cycle" "%d" cst.blocks_per_cycle;
      line "Blocks per commitment" "%d" cst.blocks_per_commitment;
      line "Blocks per roll snapshot" "%d" cst.blocks_per_roll_snapshot;
      line "Blocks per voting period" "%d" cst.blocks_per_voting_period;
      line "Time between blocks" "%d" (List.hd cst.time_between_blocks);
      line "Time between priority" "%d"
        (Misc.unopt (List.hd cst.time_between_blocks)
           (List.nth_opt cst.time_between_blocks 1));
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Rewards" ];
      line "Seed nonce revelation tip" "%Ld" cst.seed_nonce_revelation_tip;
      line "Block security deposit" "%Ld" cst.block_security_deposit;
      line "Endorsement security deposit" "%Ld" cst.endorsement_security_deposit;
      line "Block reward" "%Ld" cst.block_reward;
      line "Endorsement reward" "%Ld" cst.endorsement_reward
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Burns" ];
      line "Origination burn" "%Ld" cst.origination_burn;
      line "Cost per byte" "%Ld" cst.cost_per_byte;
      line "Hard storage limit per operation" "%Ld" cst.hard_storage_limit_per_operation
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Limits" ];
      line "Maximum revelations per block" "%d" cst.max_revelations_per_block;
      line "Maximum operation data length" "%d" cst.max_operation_data_length;
      line_option "Maximum proposals per delegate" "%d" cst.max_proposals_per_delegate;
      line "Hard gas limit per operation" "%Ld" cst.hard_gas_limit_per_operation;
      line "Hard gas limit per block" "%Ld" cst.hard_gas_limit_per_block;
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Misc" ];
      line "Proof of work nonce size" "%d" cst.proof_of_work_nonce_size;
      line "Nonce length" "%d" cst.nonce_length;
      line "Endorsers per block" "%d" cst.endorsers_per_block;
      line "Proof of work threshold" "%Ld" cst.proof_of_work_threshold;
      line "Tokens per roll" "%Ld" cst.tokens_per_roll;
      line "Michelson maximum type size" "%d" cst.michelson_maximum_type_size;
    ]
  ]

  in
  Js_utils.Manip.replaceChildren panel content
